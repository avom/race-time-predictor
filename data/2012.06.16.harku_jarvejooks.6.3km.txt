Koht	Nr	Nimi	Klubi	Aeg	Vkl	Vkl_koht	 
1	928	Tšerepannikov Sergei	Stamina Arcotransport tiim	0:19:31.0	M21	1	 
2	1	Vallimäe Viljar	Sportkeskus.ee	0:19:32.2	M21	2	 
3	2	Püi Taivo	Sportkeskus.ee	0:20:00.0	M21	3	 
4	4	Nikolajev Ilja	Jooksupartner.ee	0:20:24.4	M21	4	 
5	627	Fosti Roman	Stamina Arcotransport tiim	0:20:32.2	M21	5	 
6	3	Aristov Dmitri	SK Mitš	0:20:34.2	M21	6	 
7	607	Aus Priit	Stamina Arcotransport tiim	0:20:47.1	M21	7	 
8	663	Jürs Sander	Sportkeskus.ee	0:21:06.6	M21	8	 
9	6	Reinsalu Kristo	Stamina Arcotransport tiim	0:21:27.6	M35	1	 
10	996	Männi Allan	SK Sportkeskus.ee	0:21:33.5	M21	9	 
11	14	Orm Olari		0:21:45.1	M21	10	 
12	752	Lillelaid Tõnu		0:21:49.6	M21	11	 
13	11	Piirsalu Meelis	SIDEPATALJON	0:21:51.6	M35	2	 
14	711	Košelev Deniss	TÜ ASK	0:22:02.2	M21	12	 
15	10	Veilberg Ago	SK West Sport	0:22:13.1	M40	1	 
16	17	Kilk Ain	Eesti Worldloppeti Klubi	0:22:17.8	M40	2	 
17	260	Luik Lauri	Läänemaa KJK	0:22:35.8	M21	13	 
18	9	Salla Raimo	Türi Jõud	0:22:42.0	M21	14	 
19	844	Reitsnik Tarmo	Stamina SK	0:22:46.2	M40	3	 
20	12	Terehhov German	SK Mitš	0:22:52.0	M40	4	 
21	22	Treimuth Meelis	KJK Lõunalõvi	0:23:05.5	M40	5	 
22	28	Dzjuba Ilja	Stamina Arcotransport tiim	0:23:07.4	M21	15	 
23	20	Jaaska Allan-Peeter	Stamina SK	0:23:08.3	M40	6	 
24	238	Linkov Steven		0:23:09.8	M21	16	 
25	33	Lään Christjan	FB Jooksmine	0:23:10.6	M35	3	 
26	8	Juhkov Janar	Sportkeskus.ee	0:23:11.9	M21	17	 
27	25	Hussar Karel		0:23:33.2	M18	1	 
28	21	Laineste Andres	Stamina SK	0:23:35.2	M40	7	 
29	779	Mendel Mark	Stamina SK	0:23:36.3	M18	2	 
30	786	Moltsaar Aavo	21CC Triatloniklubi	0:23:37.6	M21	18	 
31	34	Lelov Andres		0:23:41.2	M35	4	 
32	51	Einsalu Ants	Albe Team	0:23:44.5	M50	1	 
33	731	Laanemets Raivo		0:23:46.7	M40	8	 
34	37	Enok Marek	Fauni Kaubanduse OÜ	0:23:48.2	M21	19	 
35	31	Valdmaa Lauri		0:23:55.3	M21	20	 
36	791	Murov Vjatšeslav	Murov Ski Team	0:24:03.6	M21	21	 
37	7	Laur Mikk	KJK Lõunalõvi	0:24:05.6	M21	22	 
38	50	Tiiksaar Aaro	5xA	0:24:05.9	M21	23	 
39	301	Altmets Valev		0:24:16.9	M40	9	 
40	46	Vetevoog Kaido	Stamina SK	0:24:17.3	M35	5	 
41	44	Orgulas Kaido		0:24:18.5	M40	10	 
42	27	Tilga Kajar		0:24:19.2	M40	11	 
43	59	Säkk Janar		0:24:21.2	M21	24	 
44	796	Nadel Mario	NEFAB	0:24:22.0	M21	25	 
45	592	Allikvee Hannes		0:24:23.2	M21	26	 
46	71	Kuznetsov Madis		0:24:24.9	M35	6	 
47	70	Annilo Mihkel		0:24:26.6	M21	27	 
48	720	Kuldkepp Paavo	Akrobaatikaklubi Twister	0:24:26.8	M40	12	 
49	991	Salla Marek	Türi Jõud	0:24:31.8	M21	28	 
50	171	Tasimov Sergey	SK Mits	0:24:32.7	M35	7	 
51	30	Kindel Hanno	FB Jooksmine	0:24:35.0	M40	13	 
52	32	Saar Heigo	Spordiselts	0:24:37.5	M40	14	 
53	60	Kiipsaar Raido		0:24:38.9	M40	15	 
54	588	Abner Mark	Uniprint/Jaak Mae Suusakool	0:24:40.3	M16	1	 
55	26	Einasto Mart		0:24:47.4	M40	16	 
56	939	Vaher Kaspar	SK CFC	0:24:51.5	M16	2	 
57	41	Roletsky Heiki	Logica	0:24:55.9	M35	8	 
58	45	Tammoja Andrus		0:24:56.5	M40	17	 
59	38	Järvi Esko		0:25:00.2	M21	29	 
60	67	Kamberg Kristofer Erik		0:25:03.7	M16	3	 
61	64	Atonen Marek	VAK Staier	0:25:07.9	M35	9	 
62	851	Roasto Reigo		0:25:10.1	M21	30	 
63	53	Tikva Indrek	Albe Team	0:25:12.6	M40	18	 
64	81	Enn Lauri		0:25:19.5	M21	31	 
65	135	Vahtra Märt		0:25:20.0	M21	32	 
66	1001	Trääder Helvis	Fysiopark	0:25:20.1	M21	33	 
67	86	Toom Toomas	Stamina SK	0:25:20.3	M50	2	 
68	54	Kingo Gunnar	Westline Team	0:25:24.4	M21	34	 
69	72	Lääts Viktor		0:25:27.2	M60	1	 
70	69	Ruul Rünno	Elioni Spordiklubi	0:25:30.1	M40	19	 
71	124	Lauring Karre	Kuup3 Spordiklubi	0:25:34.6	M40	20	 
72	961	Vahter Kait		0:25:35.4	M35	10	 
73	65	Sarnik Tavo		0:25:38.5	M21	35	 
74	99	Kuuskla Priit	SK Aaspere	0:25:39.0	M21	36	 
75	55	Männik Kristjan	Helmes	0:25:40.8	M21	37	 
76	68	Abner Andre	Uniprint/Stamina SK	0:25:41.5	M40	21	 
77	862	Ruus Urmas		0:25:44.5	M35	11	 
78	93	Hõbe Tarmo		0:25:49.8	M21	38	 
79	136	Pappel Jaanus		0:25:52.9	M40	22	 
80	613	Bessonov Sergei	ÅF-Automaatika	0:25:53.0	M21	39	 
81	963	Nagel Karl Gustav	SK Aaspere	0:25:54.0	M18	3	 
82	218	Gill Jakob		0:25:55.2	M18	4	 
83	96	Petraudze Andre		0:25:55.3	M21	40	 
84	812	Ots Indrek		0:26:00.1	M21	41	 
85	144	Hummal Erkki		0:26:01.7	M35	12	 
86	224	Vann Theodor		0:26:06.9	M35	13	 
87	57	Maran Tarvo	Albe Team	0:26:08.6	M40	23	 
88	738	Latsepov Leonid		0:26:10.1	M16	4	 
89	103	Mihkelson Ain	Hansa SK	0:26:10.4	M35	14	 
90	161	Moor Marko		0:26:10.4	M21	42	 
91	76	Raap Ilmar	meie	0:26:10.7	M40	24	 
92	75	Laimets Erki		0:26:11.3	M40	25	 
93	74	Kostrov Roman	Pskov Ski Team	0:26:11.9	M40	26	 
94	94	Merivälja Margus		0:26:12.8	M21	43	 
95	101	Paesüld Harles	Hansa SK	0:26:14.3	M35	15	 
96	117	Hillep Einar		0:26:19.1	M40	27	 
97	131	Pruul Heiki	Hansa SK	0:26:20.3	M21	44	 
98	232	Toom Karl	Tallinna Inglise Kolledž	0:26:21.6	M16	5	 
99	92	Pärnassalu Toomas	SS Marathon	0:26:22.4	M40	28	 
100	159	Traks Aivo	SIDEPATALJON	0:26:22.8	M40	29	 
101	187	Aave Marius	RTA Tarbatu	0:26:26.9	M21	45	 
102	870	Sander Michael		0:26:26.9	M16	6	 
103	993	Seire Indrek		0:26:27.8	M40	30	 
104	858	Roosnupp Priit		0:26:27.8	M40	31	 
105	1020	Mõistus Robert Klen	Nõmme SK	0:26:28.0	M16	7	 
106	725	Kutsar Aare		0:26:29.3	M21	46	 
107	129	Karelson Lauri		0:26:30.2	M21	47	 
108	77	Keskküla Karol		0:26:30.5	M35	16	 
109	789	Mumm Indrek		0:26:30.5	M40	32	 
110	965	Riks Rando	SK Aaspere	0:26:30.6	M21	48	 
111	120	Ponder Neeme		0:26:31.4	M50	3	 
112	89	Unt Toomas	KV Logistikakeskus	0:26:31.6	M35	17	 
113	792	Mõttus Eero		0:26:33.2	M35	18	 
114	62	Narmont Donatas	AJAKIRI JOOKSJA	0:26:34.1	M40	33	 
115	95	Liivak Hannes		0:26:34.6	M35	19	 
116	98	Alamets Ülari		0:26:36.0	M40	34	 
117	841	Rein Margus	JK Hermes	0:26:39.3	M60	2	 
118	125	Kärema Ago	adidas	0:26:41.4	M40	35	 
119	49	Rähn Alar		0:26:43.5	M21	49	 
120	122	Põldsaar Paul		0:26:44.6	M35	20	 
121	153	Pärna Ain		0:26:47.7	M35	21	 
122	944	Veeret Andreas		0:26:51.0	M21	50	 
123	108	Jõemets Aivo	Team3+	0:26:53.0	M40	36	 
124	90	Kirsipuu Avo	Eesti Worldloppeti Klubi	0:27:00.2	M40	37	 
125	189	Gussev Rain		0:27:03.1	M21	51	 
126	672	Kalev Matthias	SK CFC	0:27:04.0	M16	8	 
127	82	Pae Markus Rene	Nõmme KJK	0:27:05.6	M16	9	 
128	949	Viirmann Ott		0:27:05.6	M21	52	 
129	110	Heinsalu Raul	Albe Team	0:27:08.4	M40	38	 
130	87	Raun Rain		0:27:09.9	M40	39	 
131	150	Krilovs Ringo	Würth AS	0:27:11.7	M21	53	 
132	824	Pihklak Sander		0:27:12.2	M21	54	 
133	192	Kesküla Sulev		0:27:15.2	M40	40	 
134	973	Maiste Andrus	Springlux	0:27:15.8	M40	41	 
135	166	Sempelson Sven		0:27:18.1	M40	42	 
136	733	Laidvee Jaanus		0:27:20.2	M35	22	 
137	127	Kesküla Urmo	Leola	0:27:21.1	M35	23	 
138	830	Ponomarenko Jura	Euramax	0:27:21.2	M16	10	 
139	630	Greenbaum Janno	Rännumees	0:27:22.5	M35	24	 
140	134	Luts Vahur		0:27:23.1	M50	4	 
141	100	Nuka Innar		0:27:24.3	M21	55	 
142	644	Hämäläinen Keio		0:27:24.3	M21	56	 
143	146	Nõmmiste Sulev	Suusahullud	0:27:24.6	M40	43	 
144	80	Abner Frank	Uniprint/Jaak Mae Suusakool	0:27:25.4	M16	11	 
145	194	Reinbach Indrek		0:27:30.3	M35	25	 
146	1024	Reiter Marek		0:27:31.4	M35	26	 
147	813	Pajur Maarek	Itella	0:27:32.2	M21	57	 
148	148	Innos John		0:27:33.0	M40	44	 
149	114	Lopsik Priit	Hansa SK	0:27:34.0	M21	58	 
150	911	Tereštšenkov Aleksei	U-Klubi	0:27:36.7	M40	45	 
151	204	Tammjärv Kalle		0:27:38.1	M40	46	 
152	853	Romandi Arvi	Alavere Spordiklubi	0:27:38.6	M21	59	 
153	170	Jõeleht Jaago	Hansa SK	0:27:39.0	M35	27	 
154	155	Metsküla Üllar		0:27:41.3	M40	47	 
155	157	Mägi Indrek		0:27:41.5	M35	28	 
156	962	Kams Uku	SK Aaspere	0:27:44.2	M21	60	 
157	115	Tinast Toivo		0:27:45.7	M21	61	 
158	106	Artel Veiko		0:27:46.8	M40	48	 
159	247	Reitsak Indrek		0:27:47.3	M35	29	 
160	897	Mägi Enn		0:27:47.6	M50	5	 
161	123	Saaremäel Indrek		0:27:48.6	M35	30	 
162	910	Tepaskent Tanel		0:27:49.4	M21	62	 
163	149	Vildersen Tarmo		0:27:50.6	M21	63	 
164	104	Filatov Kalmer		0:27:53.1	M40	49	 
165	257	Tšepurov Kirill	Helmes	0:27:53.8	M21	64	 
166	821	Pere René		0:27:55.6	M40	50	 
167	198	Mustasaar Rene	Helmes	0:27:57.9	M21	65	 
168	105	Ignatenko Igor	SK SAARDE	0:27:58.6	M40	51	 
169	272	Talviste Raimo		0:28:00.4	M21	66	 
170	179	Vallner Kristjan	FB Jooksmine	0:28:00.8	M21	67	 
171	280	Kask Villu		0:28:04.8	M40	52	 
172	119	Eerme Martin		0:28:06.0	M40	53	 
173	216	Ignatenko Ingmar		0:28:07.4	M16	12	 
174	185	Reimets Ramon		0:28:07.9	M21	68	 
175	377	Tulf Taago	Samsung Sports Academy	0:28:08.5	M21	69	 
176	902	Talu Tauri	EMT	0:28:09.4	M40	54	 
177	118	Õunap Urmas		0:28:09.6	M40	55	 
178	1000	Raudsepp Ole		0:28:10.5	M21	70	 
179	1007	Sirel Andres	Jooksupartner	0:28:10.5	M21	71	 
180	173	Nokkur Lembit	Rõõsa sk	0:28:12.1	M40	56	 
181	190	Hanga Knut		0:28:12.9	M40	57	 
182	656	Juhandi Aivar	SPARTA	0:28:13.3	M35	31	 
183	884	Siiak Indrek		0:28:15.3	M35	32	 
184	140	Kaljuvee Kristo		0:28:18.9	M21	72	 
185	91	Volens Urmas		0:28:21.1	M35	33	 
186	151	Saar Laur	PharmaSwiss	0:28:21.6	M40	58	 
187	165	Kaur Jaanus	Spordiselts	0:28:24.1	M40	59	 
188	139	Grigoriev Sergei	Kaevanduste Spordiklubi	0:28:25.1	M50	6	 
189	181	Läns Indrek		0:28:27.9	M35	34	 
190	130	Kallas Mati	TSK Joosu	0:28:30.3	M50	7	 
191	299	Jaago Joel		0:28:31.4	M35	35	 
192	262	Sazenkov Aleksandr	Stamina SK	0:28:31.9	M21	73	 
193	905	Tarm Veiko	Norma	0:28:32.6	M35	36	 
194	128	Suursaar Ülo		0:28:32.8	M50	8	 
195	240	Treier Aare		0:28:34.2	M21	74	 
196	808	Oolberg Vambo		0:28:35.0	M40	60	 
197	176	Unt Marius	SPARTA	0:28:37.8	M40	61	 
198	186	Pullerits Tõnu	Spordiselts	0:28:38.5	M21	75	 
199	209	Trees Mihkel		0:28:40.0	M16	13	 
200	208	Trees Margus		0:28:40.0	M35	37	 
201	169	Ots Magnus-Mait	Leola	0:28:41.2	M40	62	 
202	719	Kulbas Oliver		0:28:42.6	M21	76	 
203	256	Englas Valdur	Tallinna Vesi	0:28:47.8	M21	77	 
204	241	Aarna Kaspar		0:28:48.8	M21	78	 
205	784	Miku Taavi	SUKL Jõulu	0:28:49.9	M40	63	 
206	97	Hollo Janno	joosu klubi	0:28:50.3	M40	64	 
207	283	Kalda Krister		0:28:51.5	M35	38	 
208	78	Kooskora Kristo		0:28:51.5	M21	79	 
209	225	Lehtmets Sven		0:28:53.5	M35	39	 
210	196	Gluhhovtsenko Maksim		0:28:55.0	M21	80	 
211	669	Kalaus Hargo		0:28:56.2	M35	40	 
212	362	Vesiallik Sten	WÜRTH	0:28:59.3	M21	81	 
213	640	Hendrikson Peeter	Haanja Rattaklubi	0:29:00.5	M40	65	 
214	175	Laanmets Jüri		0:29:01.5	M50	9	 
215	178	Rennit Leho	Albe Team	0:29:05.1	M50	10	 
216	958	Üksvärav Eido		0:29:07.8	M35	41	 
217	295	Piirsalu Rein		0:29:08.8	M60	3	 
218	609	Balõnski Janek	RahvaRattad	0:29:10.0	M35	42	 
219	810	Orumaa Raiko	Nõva SK	0:29:10.7	M21	82	 
220	955	Värton Kristjan		0:29:12.3	M35	43	 
221	219	Reimets Avo		0:29:13.2	M60	4	 
222	268	Uusma Andres	MKM	0:29:14.4	M40	66	 
223	250	Laimets Toomas		0:29:14.8	M40	67	 
224	207	Ivanov Marek		0:29:15.4	M35	44	 
225	665	Kaasik-Aaslav Henri	Vendor Eesti	0:29:15.4	M40	68	 
226	802	Nurmetalu Mehis	Vendor Eesti	0:29:15.9	M35	45	 
227	860	Ruga Rauno	Helmes	0:29:18.1	M21	83	 
228	794	Mägi Holger	Ummisjalu	0:29:20.3	M35	46	 
229	822	Peterson Kristjan		0:29:20.9	M21	84	 
230	728	Käige Rauno		0:29:22.7	M35	47	 
231	1010	Kukk Janek		0:29:23.3	M40	69	 
232	254	Veenpere Henrik	Regio	0:29:24.1	M21	85	 
233	353	Libe Taavi		0:29:24.5	M21	86	 
234	685	Kaska Marko		0:29:24.7	M35	48	 
235	242	Ahven Ahto		0:29:27.8	M18	5	 
236	142	Kruusamägi Uku		0:29:28.0	M18	6	 
237	227	Aaslaid Kaido		0:29:29.4	M40	70	 
238	174	Kin Edvin	Eesti Raudtee Spordiklubi	0:29:32.4	M16	14	 
239	273	Mäekivi Jaanus	Sinine7	0:29:33.1	M35	49	 
240	883	Shmerling Antoni	EMT	0:29:33.2	M21	87	 
241	330	Põldoja Argo		0:29:34.2	M21	88	 
242	239	Venne Andrus		0:29:34.3	M35	50	 
243	635	Hallingu Erki		0:29:34.8	M40	71	 
244	243	Kolk Marko		0:29:35.5	M40	72	 
245	1018	Tõnus Taavi	Saaremaa Vesse	0:29:37.7	M21	89	 
246	339	Valdmann Vahur		0:29:41.8	M21	90	 
247	269	Palmar Kalle	Piirivalve SKK	0:29:42.4	M60	5	 
248	230	Kukk Aalo	Eesti Panga SK	0:29:44.4	M50	11	 
249	248	Martinson Alo	SIDEPATALJON	0:29:44.4	M40	73	 
250	199	Pelapson Rain		0:29:45.5	M21	91	 
251	390	Viispert Ando	Ants Piibu bridžiklubi	0:29:46.0	M35	51	 
252	935	Urm Jaak		0:29:46.5	M35	52	 
253	356	Jürimaa Mihkel		0:29:47.3	M21	92	 
254	210	September Kevo		0:29:49.2	M21	93	 
255	638	Heil Paavo		0:29:50.2	M35	53	 
256	236	Baranov Aleksandr		0:29:51.1	M50	12	 
257	782	Merivee Matis		0:29:52.6	M21	94	 
258	229	Sakkeus Jüri		0:29:54.2	M50	13	 
259	978	Kookla Erko		0:29:56.0	M35	54	 
260	787	Mudrov Alexey	Pskov Ski Team	0:29:56.1	M50	14	 
261	1011	Valter Argo		0:29:57.2	M21	95	 
262	246	Viira Andres		0:29:58.0	M40	74	 
263	855	Roosalo Peeter		0:29:58.4	M40	75	 
264	342	Akenpärg Egert	Käkk Attack	0:29:59.6	M21	96	 
265	646	Inno Ermo	EMT	0:30:01.2	M35	55	 
266	744	Lemming Marko		0:30:02.2	M21	97	 
267	680	Karjus Kaido	ARTMAN RACING	0:30:02.8	M40	76	 
268	325	Nurmik Hans Kristjan	Elioni Spordiklubi	0:30:03.6	M18	7	 
269	634	Hallimäe Rain		0:30:04.5	M50	15	 
270	677	Kardakov Aleksandr		0:30:05.0	M50	16	 
271	780	Meos Tõnu	Shnelli SK	0:30:05.0	M50	17	 
272	670	Kaldvee Rivo		0:30:06.6	M40	77	 
273	255	Limmer Ailar		0:30:07.0	M50	18	 
274	1015	Vall Erki	Polaria	0:30:07.2	M40	78	 
275	270	Alajaan Alar		0:30:09.5	M21	98	 
276	960	Mättas Jaan	Vak Staier	0:30:10.9	M70	1	 
277	249	Ruul Mati		0:30:13.1	M50	19	 
278	228	Maasing Kaupo	Hansa SK	0:30:14.4	M21	99	 
279	278	Paur Silver	SK Aaspere	0:30:16.5	M21	100	 
280	147	Kurvits Ain	Merko Ehitus	0:30:19.9	M40	79	 
281	599	Anton Sven		0:30:20.8	M40	80	 
282	168	Pihu Tenno		0:30:23.0	M35	56	 
283	811	Osmin Martin	ATO SK	0:30:25.1	M21	101	 
284	831	Ponomarenko Juri	Euramax	0:30:25.2	M40	81	 
285	341	Maltis Jakko	Maltis	0:30:25.9	M21	102	 
286	735	Lanno Riho		0:30:28.4	M40	82	 
287	414	Lepik Rainer	Seli Tervisekeskus	0:30:30.1	M21	103	 
288	267	Lükk Villu		0:30:30.6	M21	104	 
289	683	Kasemetsa Ove Kristjan	Nõva SK	0:30:30.9	M16	15	 
290	223	Mets Olav		0:30:31.3	M21	105	 
291	396	Randver Krister	SA Keskkonnainvesteeringute Keskus	0:30:34.6	M21	106	 
292	231	Sikka Toivo		0:30:36.3	M50	20	 
293	259	Veelain Ermo		0:30:36.3	M50	21	 
294	340	Liiviste Andres		0:30:38.4	M21	107	 
295	226	Tähepõld Margus		0:30:42.1	M21	108	 
296	35	Mänd Rait	Elioni Spordiklubi	0:30:45.7	M21	109	 
297	279	Kallemets Kalev	Viru Keemia Grupp	0:30:48.1	M21	110	 
298	197	Aro Andres	kuldratas	0:30:48.3	M40	83	 
299	1012	Klaaser Gert		0:30:48.7	M40	84	 
300	297	Nigul Mihkel Erich		0:30:50.0	M16	16	 
301	837	Põtter Jaanus		0:30:50.3	M35	57	 
302	245	Toomingas Jaan		0:30:51.3	M35	58	 
303	352	Heinsaar Ants		0:30:51.3	M21	111	 
304	758	Luhaäär Karl		0:30:51.8	M21	112	 
305	304	Liivrand Eero		0:30:52.1	M40	85	 
306	936	Uusküla Lenno		0:30:53.7	M21	113	 
307	893	Sulend Herki		0:30:54.8	M35	59	 
308	306	Kin Nikolai	Eesti Raudtee Spordiklubi	0:30:56.7	M35	60	 
309	694	Kitt Markus		0:30:59.2	M18	8	 
310	908	Tensbek Taimar		0:30:59.9	M35	61	 
311	907	Tempel Marko		0:31:01.3	M21	114	 
312	162	Uksov Denis		0:31:02.7	M21	115	 
313	253	Mansberg Heigo		0:31:03.4	M35	62	 
314	318	Aasaleht Alar		0:31:04.5	M50	22	 
315	319	Mõistus Märt	Trismile	0:31:05.1	M21	116	 
316	302	Tee Dmitri	JUST DO IT	0:31:08.2	M40	86	 
317	913	Tiesel Rauno	EMT	0:31:08.7	M35	63	 
318	252	Mitrahovits Jüri	aia	0:31:11.1	M50	23	 
319	980	Koidu Sulev	Rae Kivitehas	0:31:12.5	M40	87	 
320	327	Urb Ülo		0:31:15.4	M50	24	 
321	277	Kitsing Meelis	MKM	0:31:18.0	M35	64	 
322	658	Just Alar	Team TT	0:31:18.3	M40	88	 
323	314	Karu Kaarel	P.D.Team	0:31:22.8	M35	65	 
324	921	Toming Neeme		0:31:25.1	M21	117	 
325	889	Soom Ahti		0:31:25.9	M21	118	 
326	73	Kookla Eero		0:31:26.8	M40	89	 
327	1029	Kruus Toomas		0:31:26.9	M40	90	 
328	833	Pruuel Ando		0:31:27.7	M40	91	 
329	332	Kello Gert	Logica	0:31:29.2	M35	66	 
330	801	Nurk Erki		0:31:34.4	M40	92	 
331	915	Tiivoja Taivo		0:31:36.9	M40	93	 
332	265	Kakko Ard-Kristian		0:31:38.7	M40	94	 
333	331	Arutyunyan David		0:31:39.1	M21	119	 
334	755	Lindsalu Indrek		0:31:40.9	M50	25	 
335	258	Hanga Karol		0:31:42.6	M16	17	 
336	899	Särak Ivo	Running Expert	0:31:43.8	M35	67	 
337	282	Lepik Arto	FB Jooksmine	0:31:51.0	M21	120	 
338	848	Rikkinen Tarmo	SWECO	0:31:51.9	M40	95	 
339	702	Kivisild Tiit		0:31:52.3	M50	26	 
340	307	Kannike Arnold		0:31:52.7	M35	68	 
341	357	Vaks Toomas	Domus Kinnisvara	0:31:53.3	M21	121	 
342	591	Allekand Ingvar	Domus Kinnisvara	0:31:53.7	M35	69	 
343	1004	Veering Priit		0:31:56.2	M35	70	 
344	885	Silde Annes		0:31:56.6	M21	122	 
345	266	Harkov Andrei		0:31:56.8	M21	123	 
346	335	Luus Arvo		0:31:58.5	M70	2	 
347	633	Habicht Jarno		0:32:00.7	M35	71	 
348	281	Rohtma Valdek		0:32:01.6	M21	124	 
349	749	Levandi Mauri		0:32:02.5	M18	9	 
350	383	Kompus Andrus		0:32:08.8	M50	27	 
351	303	Rahula Margus		0:32:10.8	M40	96	 
352	814	Pajuste Heiki	HP-Sport team	0:32:11.2	M40	97	 
353	338	Jesse Riho		0:32:13.3	M40	98	 
354	764	Lõhmus Mikk		0:32:13.8	M35	72	 
355	688	Kiilaspää Ants	Vendor Eesti	0:32:14.3	M35	73	 
356	423	Ots Aivar	Samsung Sports Academy	0:32:15.8	M21	125	 
357	275	Nokkur Ants	Rõõsa sk	0:32:17.7	M21	126	 
358	336	Malv Madis		0:32:19.4	M35	74	 
359	1026	Rist Karlo		0:32:19.5	M21	127	 
360	313	Nilson Koit		0:32:19.8	M50	28	 
361	386	Mauer Tiit		0:32:24.5	M40	99	 
362	358	Lätte Oliver		0:32:25.3	M21	128	 
363	404	Väär Kaimar		0:32:25.8	M21	129	 
364	701	Kivisild Andres		0:32:26.9	M18	10	 
365	890	Soom Priit		0:32:32.0	M35	75	 
366	992	Abel Renee		0:32:35.2	M40	100	 
367	359	Markson Uno		0:32:35.9	M50	29	 
368	431	Kiiler Meelis		0:32:36.0	M21	130	 
369	323	Lopsik Andrei	Eesti Energia Spordiklubi	0:32:36.3	M50	30	 
370	715	Kruus Kaarel		0:32:37.5	M21	131	 
371	867	Sakkeus Kristo		0:32:38.3	M21	132	 
372	879	Sepa Aimar	Samsung Sports Academy	0:32:40.8	M21	133	 
373	1025	Ross Priit		0:32:42.9	M21	134	 
374	723	Kunman Jarek-Karl		0:32:43.6	M16	18	 
375	317	Soolo Mati		0:32:46.9	M35	76	 
376	729	Künnapas Indrek		0:32:47.7	M35	77	 
377	437	Lümat Alo		0:32:52.6	M35	78	 
378	706	Kokmann Imre		0:32:53.2	M21	135	 
379	305	Alajaan Andres	EMT	0:32:54.9	M60	6	 
380	416	Liiv Daimar	Liivide SK	0:32:55.4	M40	101	 
381	847	Rikker Karl-Steven		0:32:58.1	M16	19	 
382	395	Metsa Erkki		0:32:59.0	M35	79	 
383	959	Pärtma Enn		0:33:00.9	M50	31	 
384	345	Parts Veiko		0:33:02.3	M40	102	 
385	1027	Metsaorg Jüri	Maria Jooksuklubi	0:33:04.0	M40	103	 
386	348	Isakov Viktor	Pskov Ski Team	0:33:04.6	M50	32	 
387	322	Kamberg Peeter		0:33:05.3	M40	104	 
388	409	Meltsas Veiko		0:33:07.6	M35	80	 
389	435	Veimen Ivo		0:33:09.4	M21	136	 
390	868	Saks Raimond	Elioni SK	0:33:10.8	M50	33	 
391	982	Reintam Juhto		0:33:12.2	M40	105	 
392	454	Kurg Raimo		0:33:12.7	M21	137	 
393	953	Väljaots Kain		0:33:13.8	M35	81	 
394	655	Jevtušenko Andrei		0:33:14.0	M21	138	 
395	447	Gill Paul		0:33:14.8	M40	106	 
396	388	Pähklamäe Ville		0:33:14.9	M18	11	 
397	406	Pähklamäe Viktor		0:33:15.0	M40	107	 
398	351	Kallas Krister	TSK Joosu	0:33:19.3	M21	139	 
399	354	Mändla Raul		0:33:19.8	M40	108	 
400	329	Nurmik Karl Kristjan	Elioni Spordiklubi	0:33:20.9	M18	12	 
401	429	Nurmik Andrus	Elioni Spordiklubi	0:33:21.2	M40	109	 
402	693	Kitt Aimar		0:33:21.7	M40	110	 
403	182	Karpovitch Sergei		0:33:23.1	M35	82	 
404	469	Kivro Erko		0:33:23.3	M21	140	 
405	138	Vinogradov Vladimir	SK T28A	0:33:23.4	M40	111	 
406	999	Kull Jako		0:33:24.1	M50	34	 
407	1005	Tammela Paivo		0:33:26.0	M40	112	 
408	684	Kasesalu Tanel	Samsung Sports Academy	0:33:28.1	M21	141	 
409	600	Antso Imre		0:33:29.2	M21	142	 
410	156	Rüütelmaa Andrus	KV Logistikakeskus	0:33:30.7	M40	113	 
411	785	Moglia Paolo		0:33:33.1	M40	114	 
412	413	Voolaine Andrus	GRTC Eesti	0:33:35.5	M50	35	 
413	699	Kivioja Martin		0:33:35.7	M21	143	 
414	998	Teplõhh Dmitri		0:33:36.3	M21	144	 
415	587	Aaslaid Kenno		0:33:36.9	M16	20	 
416	948	Viira Toomas		0:33:37.2	M35	83	 
417	920	Tolli Rivo	Samsung Sports Academy	0:33:38.0	M21	145	 
418	598	Anton Meelis	Elioni Spordiklubi	0:33:46.4	M21	146	 
419	732	Laanesaar Kaur	Samsung Sports Academy	0:33:47.1	M21	147	 
420	442	Maurer Raim	SK Aaspere	0:33:49.3	M18	13	 
421	403	Pajusalu Jaanus		0:33:51.6	M35	84	 
422	852	Rohde Ronald		0:33:53.5	M35	85	 
423	493	Istsenko Sergei	JK START	0:33:55.3	M50	36	 
424	745	Lemming Tarmo		0:33:57.4	M21	148	 
425	374	Vahtrik Valdo		0:33:58.8	M21	149	 
426	349	Kristin Toomas		0:34:00.5	M40	115	 
427	264	Saar Siim		0:34:01.2	M50	37	 
428	877	Seil Tõnu	adidas	0:34:02.1	M35	86	 
429	876	Schweigert Erwin	ESC	0:34:03.6	M21	150	 
430	389	Ahman Lenno		0:34:05.9	M35	87	 
431	452	Leek Tarmo		0:34:07.9	M50	38	 
432	367	Lõhmus Raul	EMT Topeltpluss klient	0:34:10.6	M40	116	 
433	1030	Uibo Raido		0:34:21.2	M21	151	 
434	436	Habicht Antero		0:34:21.6	M21	152	 
435	624	Erik Hans	SK Saue Tammed	0:34:28.3	M16	21	 
436	102	Kirsipuu Riho	Audentes	0:34:28.7	M16	22	 
437	488	Parri Erki		0:34:35.5	M21	153	 
438	916	Tikva Daniel		0:34:37.5	M16	23	 
439	990	Arvi Fred	Nõmme SK	0:34:39.4	M16	24	 
440	766	Lääne Maidu		0:34:40.0	M60	7	 
441	1008	Lenk Ivo		0:34:42.1	M35	88	 
442	773	Matsar Raul		0:34:43.6	M40	117	 
443	894	Sulev Janek		0:34:46.9	M21	154	 
444	997	Turkin Viktor	HLO	0:34:50.7	M40	118	 
445	671	Kalev Christopher	SK CFC	0:34:51.4	M16	25	 
446	1028	Kass Argo		0:34:52.8	M40	119	 
447	843	Reiska Hando	Vendor Eesti	0:34:58.0	M70	3	 
448	1002	Tiitto Mikk		0:34:59.3	M21	155	 
449	470	Loot Avo		0:35:00.9	M50	39	 
450	421	Lehtmets Janek	Effort Capital OÜ	0:35:01.3	M35	89	 
451	528	Tiits Toomas	Samsung Sports Academy	0:35:02.0	M35	90	 
452	859	Roots Anto		0:35:02.0	M40	120	 
453	486	Lepasaar Ivo		0:35:05.5	M40	121	 
454	799	Niit Jaak		0:35:08.2	M40	122	 
455	595	Amur Rainer		0:35:08.2	M21	156	 
456	690	Kiivit Tarmo	Helmes	0:35:08.6	M21	157	 
457	742	Lell Alari	Nike / AS Jalajälg	0:35:10.9	M35	91	 
458	763	Luts Vello	ABB	0:35:11.6	M35	92	 
459	648	Jaakson Jüri	Saaremaa Golfiklubi	0:35:12.0	M40	123	 
460	829	Poikalainen Stevo	Samsung Sports Academy	0:35:12.9	M21	158	 
461	365	Balõnski Peeter		0:35:18.7	M60	8	 
462	425	Voolaid Vello		0:35:26.4	M40	124	 
463	448	Kukk Andrus	WÜRTH	0:35:27.3	M40	125	 
464	871	Sarap Martin	Samsung Sports Academy	0:35:29.3	M35	93	 
465	806	Oago Reino		0:35:30.8	M21	159	 
466	661	Jõeäär Tanel		0:35:31.8	M21	160	 
467	465	Nemvalts Margus		0:35:32.3	M21	161	 
468	361	Kollo Imre		0:35:38.5	M35	94	 
469	710	Kosula Tidrik		0:35:47.8	M40	126	 
470	631	Haan Joel	Elisa SK	0:35:49.8	M35	95	 
471	807	Oja Andres	adidas	0:35:51.6	M40	127	 
472	517	Sadrak Erkki	EMT	0:35:57.4	M40	128	 
473	988	Ehala Eduard	Profencinteam	0:36:09.3	M35	96	 
474	816	Parmas Aalo		0:36:13.8	M50	40	 
475	832	Proos Almar		0:36:14.1	M40	129	 
476	371	Maripuu Madis		0:36:15.8	M40	130	 
477	508	Engel Margo		0:36:20.4	M35	97	 
478	805	Nõmmiste Siim		0:36:21.9	M16	27	 
479	504	Lõhmus Viktor	Domus Kinnisvara	0:36:22.4	M40	131	 
480	468	Kurik Arvi		0:36:25.3	M21	163	 
481	697	Kivikangur Raido		0:36:25.3	M21	162	 
482	754	Lindeman Tõnu	Vendor Eesti	0:36:27.5	M21	164	 
483	957	Ääremaa Andres		0:36:32.1	M35	98	 
484	415	Trofimov Ove	Trismile	0:36:43.4	M21	165	 
485	820	Pelisaar Ago	MKM	0:36:45.6	M21	166	 
486	424	Riidamets Martin		0:36:46.8	M50	41	 
487	1017	Tael Sulev		0:36:48.7	M40	132	 
488	472	Bluum Tomas	KV Logistikakeskus	0:36:49.9	M35	99	 
489	900	Taaler Janek	adidas	0:36:54.0	M35	100	 
490	865	Saar Roomand	Domus Kinnisvara	0:36:54.7	M40	133	 
491	649	Jaanisoo Ülo		0:36:55.2	M35	101	 
492	718	Kukk Tanel		0:36:56.5	M18	14	 
493	441	Vatsel Rigo		0:36:59.4	M40	134	 
494	133	Kingo Priit	Hansa SK	0:37:03.6	M35	102	 
495	1021	Pihu Rasmus	Viljandi SK	0:37:09.2	M16	29	 
496	709	Kostjukov Jaan		0:37:23.1	M21	167	 
497	906	Teekivi Tanel		0:37:27.9	M21	168	 
498	446	Kliimann Raivo	BHRA	0:37:39.2	M40	135	 
499	622	Ennok Oliver		0:37:47.2	M35	103	 
500	549	Uksov Andrei		0:37:52.5	M21	169	 
501	471	Laretei Tarmo		0:37:59.3	M60	9	 
502	662	Jürisson Mihkel		0:38:04.6	M35	104	 
503	774	Matsar Robert Raul		0:38:11.1	M16	30	 
504	419	Ausmees Toomas		0:38:14.4	M40	136	 
505	777	Matvere Raimo		0:38:17.5	M21	170	 
506	457	Kranfeldt Kaido-Paul		0:38:22.0	M40	137	 
507	514	Stallmeister Argo		0:38:22.8	M21	171	 
508	1006	Suurorg Boris		0:38:29.2	M60	10	 
509	292	Kane Rein	Loksa RSK	0:38:37.3	M60	11	 
510	941	Vaino Indrek		0:38:39.1	M35	105	 
511	687	Keba Andres	BestEnergy OÜ	0:38:41.9	M35	106	 
512	1031	Heinla Ergo	Vendur	0:38:42.2	M35	107	 
513	422	Suislepp Peeter		0:38:48.4	M40	138	 
514	544	Jaago Aivar		0:39:04.9	M50	42	 
515	979	Järva Matti		0:39:13.0	M60	12	 
516	497	Parrest Indrek		0:39:14.9	M35	108	 
517	542	Liba Erkki		0:39:46.3	M35	109	 
518	983	Sirk Adolf	Nõmme	0:40:12.9	M70	4	 
519	495	Rajamägi Priit		0:40:30.0	M35	110	 
520	798	Neeme Indrek		0:40:40.2	M21	172	 
521	682	Karuauk Kaupo		0:40:41.4	M21	173	 
522	778	Meigas Meelis		0:40:41.8	M21	174	 
523	790	Murel Erki	EMT	0:40:59.8	M35	111	 
524	548	Joa Olev		0:41:01.3	M70	5	 
525	995	Pais Robert		0:41:02.0	M16	31	 
526	597	Anni Mihkel		0:41:30.4	M50	43	 
527	418	Valjala Veiko		0:42:15.3	M35	112	 
528	560	Nokkur Arno	Rõõsa sk	0:43:31.5	M16	32	 
529	986	Maripuu Mark		0:43:46.6	M16	33	 
530	626	Fimberg Tarmo		0:43:48.5	M50	44	 
531	533	Kalu Aivo		0:43:48.8	M21	175	 
532	650	Jahilo Jaan		0:44:17.3	M16	34	 
533	566	Mänd Ranel	Hansa SK	0:44:26.7	M21	176	 
534	950	Viisitam Ants		0:44:29.0	M35	113	 
535	1022	Maksimov Juri		0:44:37.7	M40	139	 
536	657	Juse Martti	Domus Kinnisvara	0:45:00.3	M21	177	 
537	964	Nagel Georg Harald	SK Aaspere	0:45:04.2	M16	35	 
538	214	Nõmmiste Kalev		0:45:59.1	M40	140	 
539	572	Postnikov Nikolai	JK START	0:46:07.0	M70	6	 
540	567	Raudpuu Madis	Ummisjalu	0:46:15.6	M21	178	 
541	945	Veli Jaanus	Vendor Eesti	0:49:12.4	M40	141	 
542	610	Balõnski Joosep	RahvaRattad	0:35:07.6	M16	26	 
543	716	Kruusimaa Karl		0:55:12.5	M21	179	 
544	835	Põder Kaido		0:55:20.0	M21	180	 
545	681	Karu Veiko		0:55:26.4	M21	181	 
546	641	Hindrea Ahto		0:55:47.1	M50	45	 
547	580	Kõre Aivar		0:55:47.8	M40	142	 
548	620	Ellam Jan Jorke	EMT	0:36:49.6	M16	28	 
549	977	Šmurov Dmitri		1:02:57.9	M70	7	 
550	483	Tamm Taavi		1:05:33.8	M21	182	 
551	803	Nurmetalu Rene	Vendor Eesti	1:18:19.4	M40	143	 
1	15	Talts Evelin	Stamina Arcotransport tiim	0:22:55.4	N35	1	 
2	42	Mägi Merill	Stamina Arcotransport tiim	0:24:18.3	N21	1	 
3	29	Andrejeva Olga	Stamina Arcotransport tiim	0:24:24.1	N21	2	 
4	48	Tarum Janelle	Stamina Arcotransport tiim	0:25:27.9	N21	3	 
5	875	Savtšenko Ksenia	KJK Kalev Sillamäe	0:26:06.5	N16	1	 
6	116	Vaher Annika	SJK Sarma	0:26:07.5	N40	1	 
7	1014	Kukk Kaisa	Stamina SK	0:26:14.2	N21	4	 
8	891	Soots Viivi-Anne	SK Saue Tammed	0:26:21.3	N40	2	 
9	79	Teppo Anu	Sportkeskus.ee	0:26:27.2	N21	5	 
10	288	Kurist Helina	SK EMÜ	0:26:54.6	N18	1	 
11	776	Matson Kaire	Elujooks	0:27:47.9	N21	6	 
12	172	Tulp Tiia		0:27:50.1	N40	3	 
13	167	Randmaa Katri		0:28:03.8	N35	2	 
14	113	Sauga Mari		0:28:05.4	N40	4	 
15	244	Kuiv Kadiliis		0:28:13.9	N21	7	 
16	211	Parts Valli		0:28:18.1	N21	8	 
17	132	Bernat Galina	Stamina SK	0:28:25.9	N50	1	 
18	586	Aarna Gertrud	SK CFC	0:28:26.6	N18	2	 
19	202	Lees Loviisa	Nõmme Spordiklubi	0:28:30.1	N16	2	 
20	206	Jürisaar Merilin		0:28:36.6	N16	3	 
21	177	Muru Merike		0:28:53.8	N40	5	 
22	576	Güsson Sandra		0:28:58.7	N16	4	 
23	263	Kummer-Leman Ülle	Sportkeskus.ee	0:29:08.6	N50	2	 
24	233	Laev Nele	EMT	0:29:09.2	N21	9	 
25	712	Kresmer Kristiina	SK CFC	0:29:14.2	N16	5	 
26	284	Toomingas Terje	Stamina SK	0:29:30.1	N21	10	 
27	158	Ehrlich Imbi		0:29:37.0	N40	6	 
28	605	Arro Liis-Grete	Stamina Arcotransport tiim	0:29:46.1	N21	11	 
29	160	Kalda Ruth	TÜ ASK	0:29:48.6	N40	7	 
30	221	Sillanurm Ly		0:29:53.5	N40	8	 
31	220	Mõistus Merlin		0:29:56.4	N21	12	 
32	180	Vulla Ela	Saksi	0:29:58.9	N21	13	 
33	589	Ahtiäinen Elina	SK CFC	0:30:10.0	N16	6	 
34	261	Treffner Kadri	Kõlleste Spordiklubi	0:30:18.5	N21	14	 
35	612	Bergman Gina	SPARTA	0:30:18.8	N21	15	 
36	315	Pellenen Evelin		0:30:25.4	N35	3	 
37	309	Valter Järvi		0:30:28.2	N50	3	 
38	312	Rammula Kädi		0:30:32.1	N21	16	 
39	815	Pajuste Helis	HP-Sport team	0:30:41.5	N16	7	 
40	290	Jõemets Kaja	SK Mitš	0:30:45.6	N40	9	 
41	286	Helmja Kati		0:30:51.6	N21	17	 
42	713	Krieger Margit		0:30:54.5	N40	10	 
43	382	Kangur Kadri	Vendor Eesti	0:31:00.8	N21	18	 
44	291	Uusküla Liis	Männikumägi	0:31:05.2	N18	3	 
45	350	Jürjo Helen	Elioni Spordiklubi	0:31:08.0	N21	19	 
46	1016	Kapsta Anneli	Sparta	0:31:10.5	N40	11	 
47	320	Belokurova Eve		0:31:11.6	N50	4	 
48	372	Rets Pille		0:31:18.0	N21	20	 
49	985	Ilver Ene	Rulluisuklubi ABEC	0:31:19.4	N35	4	 
50	234	Aun Eret	MTA SK	0:31:23.2	N21	21	 
51	1023	Karu Inga		0:31:27.0	N40	12	 
52	300	Kindlam Jaanika	Elioni Spordiklubi	0:31:27.7	N35	5	 
53	434	Sillaots Kristel	Fauni Kaubanduse OÜ	0:31:28.1	N35	6	 
54	385	Talts Eve	EMT	0:31:29.8	N35	7	 
55	762	Luts Ege		0:31:31.3	N21	22	 
56	366	Kooskora Triinu		0:31:35.3	N21	23	 
57	621	Enn Kairi	Elioni SK	0:31:48.2	N21	24	 
58	274	Kruusmann Geidi		0:31:49.2	N21	25	 
59	898	Säkk Relika		0:31:53.6	N21	26	 
60	334	Gladõsheva Galina	Kaevanduste Spordiklubi	0:31:56.4	N50	5	 
61	623	Erik Birgit		0:32:00.6	N21	27	 
62	476	Koplimägi Marika		0:32:01.0	N40	13	 
63	914	Tiidermann Age		0:32:08.6	N35	8	 
64	276	Jägel Kadri		0:32:12.0	N21	28	 
65	926	Truve Elizabeth		0:32:13.3	N16	8	 
66	438	Saar Annely	Saku Õlletehase SK	0:32:22.2	N35	9	 
67	402	Lillemägi Eeva		0:32:23.6	N21	29	 
68	324	Jõemets Kristi	Team 3+	0:32:27.6	N21	30	 
69	355	Kose Evelin		0:32:32.2	N35	10	 
70	759	Luhaäär Reet		0:32:32.2	N50	6	 
71	369	Neidorf Kerttu		0:32:35.1	N21	31	 
72	310	Rennit Triin	Albe Team	0:32:36.7	N16	9	 
73	379	Laikre Katrin		0:32:40.1	N40	14	 
74	722	Kulpas Anna-Liis	SK CFC	0:32:47.0	N16	10	 
75	380	Pihel Aimi	MTA SK	0:32:51.2	N60	1	 
76	316	Velba Helena		0:32:54.2	N21	32	 
77	343	Roopärg Marika		0:32:57.4	N40	15	 
78	397	Keldu Maiken		0:33:07.6	N35	11	 
79	381	Riener Gitta		0:33:10.8	N21	33	 
80	770	Malv Esther		0:33:22.0	N21	34	 
81	826	Pindis Nele	Samsung Sports Academy	0:33:26.9	N21	35	 
82	937	Uusküla Mari		0:33:31.0	N21	36	 
83	602	Areng Liina		0:33:35.7	N35	12	 
84	400	Kask Kaisa		0:33:36.9	N21	37	 
85	632	Haarde Maiken	adidas	0:33:46.8	N21	38	 
86	384	Jesse Karin		0:33:47.8	N40	16	 
87	443	Eller Eike		0:33:48.5	N40	17	 
88	433	Puuorg Helina		0:33:49.2	N35	13	 
89	394	Siilivask Kaie		0:33:50.0	N21	39	 
90	387	Terro Evelin	EMT	0:33:52.8	N21	40	 
91	333	Tilga Kaari		0:33:53.5	N18	4	 
92	410	Seljak Katrin		0:33:54.8	N40	18	 
93	440	Kubo Kitty		0:33:56.0	N40	19	 
94	1013	Lehtoja Eha		0:33:57.6	N50	7	 
95	363	Ojasu Jane		0:33:59.4	N21	41	 
96	464	Kriisa Merili		0:34:04.7	N21	42	 
97	428	Getman Ljudmilla	JK START	0:34:14.0	N50	8	 
98	981	Hiiemäe Ursula	FIRN	0:34:20.8	N35	14	 
99	459	Toodu Kersti		0:34:23.8	N50	9	 
100	391	Volens Maret	KPME	0:34:24.8	N40	20	 
101	449	Nõmmiste Leie		0:34:40.5	N35	15	 
102	975	Talu Juta		0:34:41.2	N60	2	 
103	368	Tramm Maire		0:34:42.5	N40	21	 
104	420	Samblik Eela		0:34:43.2	N21	43	 
105	956	Väärtnõu Katrin		0:34:44.6	N21	44	 
106	886	Silm Getter		0:34:45.0	N16	11	 
107	923	Topp Mariliis Mia	Tallinna Vesi	0:34:52.5	N21	45	 
108	487	Täär Karin		0:34:53.2	N21	46	 
109	439	Saar Signe		0:34:54.7	N35	16	 
110	412	Kööp Kadri		0:35:00.8	N35	17	 
111	804	Nõmmiste Maris		0:35:06.9	N16	12	 
112	456	Vaabel Kirsti		0:35:09.3	N40	22	 
113	298	Naisson Gerlin	adidas	0:35:11.2	N21	47	 
114	666	Kadari Liina	adidas	0:35:11.3	N21	48	 
115	819	Peial Evelin		0:35:14.8	N40	23	 
116	714	Krukovskaja Regina	Samsung Sports Academy	0:35:28.9	N21	49	 
117	411	Talihärm Maire	Eesti Raudtee SK	0:35:30.5	N40	24	 
118	445	Piir Evelin		0:35:32.5	N21	50	 
119	491	Mauer Ülle		0:35:33.8	N40	25	 
120	432	Nõmmik Annika		0:35:34.4	N21	51	 
121	427	Mägi Triin		0:35:36.2	N21	52	 
122	579	Loolaid-Raudpuu Eva	Logica	0:35:39.8	N21	53	 
123	809	Orav Juula	HP-Sport team	0:35:47.8	N35	18	 
124	825	Piigli Marta	Vendor Eesti	0:35:52.2	N21	54	 
125	511	Pisa Ave		0:35:53.3	N35	19	 
126	927	Trääder Ruth		0:35:56.0	N21	55	 
127	477	Rahe Laura	AJAKIRI JOOKSJA	0:36:14.1	N21	56	 
128	475	Erm Eveliine		0:36:17.7	N40	26	 
129	863	Rõngelep Maris	Audentes	0:36:19.2	N21	57	 
130	462	Viira Maret		0:36:20.4	N40	27	 
131	485	Marand Monika		0:36:21.2	N35	20	 
132	603	Arovald Diore		0:36:23.6	N21	58	 
133	686	Kaupmees Kati	Samsung Sports Academy	0:36:30.3	N35	21	 
134	929	Tuuletare Kai	Samsung Sports Academy	0:36:32.3	N21	59	 
135	543	Pals Anne		0:36:34.1	N60	3	 
136	463	Nokkur Kadri	Rõõsa sk	0:36:47.1	N21	60	 
137	461	Reisel Asta	DB Schenker	0:36:48.8	N40	28	 
138	347	Aruvald Kairi	Elioni Spordiklubi	0:36:50.9	N21	61	 
139	601	Apart Tiina	BDK	0:36:54.6	N35	22	 
140	506	Vallner Katre	FB Jooksmine	0:36:56.6	N35	23	 
141	450	Puks Krista		0:36:58.6	N21	62	 
142	489	Parbo Marge	SPARTA	0:37:00.5	N40	29	 
143	474	Aaring Kerttu		0:37:04.3	N21	63	 
144	473	Nurmis Pille	Elioni Spordiklubi	0:37:06.5	N50	10	 
145	398	Mulla Kaja	Stamina SK	0:37:13.7	N40	30	 
146	426	Ollino Eva	Jooksupartner	0:37:14.7	N21	64	 
147	903	Tamm Siiri	Elioni Spordiklubi	0:37:15.4	N21	65	 
148	408	Leif Kristel	Hansa SK	0:37:18.2	N21	66	 
149	637	Heil Kersti		0:37:20.2	N35	24	 
150	989	Raun Marianne	Nõmme SK	0:37:24.0	N16	13	 
151	940	Vaik Mariane		0:37:24.1	N21	67	 
152	405	Laretei Riina	Elioni Spordiklubi	0:37:27.7	N50	11	 
153	520	Reintop Marika		0:37:34.5	N21	68	 
154	574	Kuningas Ingrid	SK Saue Tammed	0:37:35.4	N40	31	 
155	704	Koch-Mäe Janika	SPARTA	0:37:38.9	N21	69	 
156	519	Jaago Külli	FB Jooks	0:37:39.5	N35	25	 
157	721	Kull Kristel		0:37:42.9	N21	70	 
158	501	Sabolotni Pille		0:37:43.6	N40	32	 
159	904	Tammepõld Marika		0:37:48.5	N40	33	 
160	510	Kaasik Kairit		0:37:53.2	N40	34	 
161	524	Gross Triinu		0:38:02.8	N35	26	 
162	590	Aigro Jaana		0:38:04.4	N35	27	 
163	479	Valdmaa Hille		0:38:05.6	N60	4	 
164	740	Lausma-Saar Helen		0:38:07.1	N21	71	 
165	494	Luuk Epp	Hansa SK	0:38:10.2	N40	35	 
166	481	Käen Piret	Samsung Sports Academy	0:38:17.2	N21	72	 
167	451	Peebo Margit	Stamina SK	0:38:19.3	N40	36	 
168	482	Lille Tiina		0:38:26.1	N21	73	 
169	503	Kangro Merike	Tartu Inline tiim	0:38:32.6	N21	74	 
170	741	Leibur Epp		0:38:38.7	N50	12	 
171	534	Nisu Kadi	Tartu Inline team	0:38:43.1	N21	75	 
172	507	Sadrak Eha		0:38:46.8	N40	37	 
173	966	Kalevik Tiina	Trismile	0:38:50.8	N21	76	 
174	540	Talviste Britt		0:38:58.7	N21	77	 
175	924	Treier Kai		0:39:02.2	N40	38	 
176	526	Anton Annika		0:39:04.9	N40	39	 
177	466	Liiv Annika	Liivide SK	0:39:06.8	N21	78	 
178	546	Kaljulaid Maria		0:39:08.0	N21	79	 
179	478	Kollo Merle		0:39:10.8	N35	28	 
180	842	Reinaste-Parve Katrin		0:39:12.5	N40	40	 
181	698	Kivimägi Ly		0:39:17.6	N35	29	 
182	771	Mamers Kitty		0:39:18.0	N21	80	 
183	541	Rajamägi Siiri		0:39:28.7	N21	81	 
184	500	Vainumaa Kaili		0:39:31.9	N40	41	 
185	969	Neeme Kadi	Samsung Sports Academy	0:39:32.8	N21	82	 
186	484	Alajaan Aire		0:39:38.3	N21	83	 
187	994	Boman Kat		0:39:38.9	N21	84	 
188	717	Ksentitskaja Ksenia		0:39:41.9	N21	85	 
189	756	Lippe Sirje		0:39:43.5	N40	42	 
190	827	Pintmann Elisabeth	SK CFC	0:39:50.0	N16	14	 
191	746	Leo Terje		0:39:51.3	N40	43	 
192	518	Trummal Vilma		0:39:52.9	N50	13	 
193	753	Lillmann Sirli		0:40:00.6	N21	86	 
194	502	Timmusk Kai		0:40:03.9	N40	44	 
195	1019	Valkenklau Nele		0:40:06.7	N21	87	 
196	765	Lääne Kaily		0:40:10.3	N21	88	 
197	695	Kitt Marleen		0:40:22.7	N21	89	 
198	611	Balõnski Urve	RahvaRattad	0:40:32.7	N60	5	 
199	970	Rull Monika	Trismile	0:40:34.9	N18	5	 
200	547	Zahharova Oksana		0:40:37.4	N21	90	 
201	912	Tereštšenkova Laada		0:40:42.1	N16	15	 
202	909	Tenter Mari-Ann	HP-Sport team	0:40:42.8	N35	30	 
203	676	Kann Kadri		0:40:45.6	N21	91	 
204	783	Mets Margit		0:40:48.3	N35	31	 
205	455	Merivälja Moonika		0:40:52.8	N35	32	 
206	696	Kivi Katrin	SIDEPATALJON	0:40:53.9	N21	92	 
207	917	Tobias Eve		0:40:54.4	N40	45	 
208	737	Larionova Diana		0:40:55.0	N21	93	 
209	747	Leps Merle		0:40:58.3	N21	94	 
210	984	Hallingu Simone		0:41:03.1	N16	16	 
211	608	Bachmann Ketlin	Elisa SK	0:41:06.8	N21	95	 
212	552	Lemendik Helena		0:41:15.9	N40	46	 
213	878	Sempelson Eda		0:41:16.6	N40	47	 
214	615	Crawford Rachel		0:41:17.4	N21	96	 
215	538	Luhaväli Katrin		0:41:34.4	N21	97	 
216	705	Koha Mariell	Vendor Eesti	0:41:43.5	N21	98	 
217	976	Männamaa Siiri		0:41:53.8	N40	48	 
218	498	Everst Kristiina	KV Logistikakeskus	0:41:58.9	N21	99	 
219	529	Arula Karolin		0:42:00.2	N35	33	 
220	974	Mustonen Liina	Springlux	0:42:00.7	N35		 
221	781	Merivee Anni		0:42:13.0	N21	100	 
222	987	Ehala Anna-Helena	Audentese SK	0:42:27.3	N16	17	 
223	748	Levandi Maigi		0:42:40.4	N40	49	 
224	645	Ignatenko Silja		0:42:41.4	N40	50	 
225	817	Parrest Nele		0:42:42.7	N21	101	 
226	581	Amos Mari		0:42:42.8	N35	34	 
227	772	Matsar Kaidi		0:42:46.9	N40	51	 
228	558	Vaadumäe Raili	Itella	0:42:47.8	N18	6	 
229	972	Talve Keiu	Trismile	0:43:11.7	N21	102	 
230	845	Rennig Valli		0:43:12.5	N21	103	 
231	564	Zguro Emma	JK START	0:43:14.3	N50	14	 
232	553	Maidle Este		0:43:31.1	N50	15	 
233	736	Lapõševa Ksneia		0:43:35.2	N21	104	 
234	515	Saulep Merilyn		0:43:35.8	N21	105	 
235	757	Lips Kristel		0:43:58.4	N21	106	 
236	730	Laagus Laura	SPARTA	0:43:58.9	N16	18	 
237	556	Parik Ilme		0:44:14.6	N40	52	 
238	971	Rull Mari	Trismile	0:44:16.5	N35	35	 
239	888	Sinihelm Chris		0:44:32.7	N21	107	 
240	734	Laineste Mairit	Vanamõisa SK	0:44:33.1	N21	108	 
241	726	Kõo Katrin		0:44:34.6	N21	109	 
242	568	Luukas Ülle		0:44:38.9	N50	16	 
243	545	Heinmäe Rage		0:44:46.2	N21	110	 
244	750	Liiv Kristiina	Liivide SK	0:44:46.5	N16	19	 
245	539	Timmusk Kirke		0:44:47.9	N16	20	 
246	919	Tofert-Kommusaar Merilin	Helmes	0:44:49.7	N21	111	 
247	554	Rohi Ly		0:44:52.0	N21	112	 
248	947	Viilup Egne	NEFAB	0:44:57.0	N21	113	 
249	569	Sellak-Martinson Saima		0:45:02.7	N35	36	 
250	571	Selge Enge		0:45:31.6	N21	114	 
251	619	Eliste Kristel		0:45:31.8	N21	115	 
252	565	Laanmets Kai		0:45:33.8	N21	116	 
253	570	Liik Maarika		0:45:37.8	N21	117	 
254	527	Luik Chris-Evelin		0:45:42.2	N21	118	 
255	946	Vene Maret		0:46:12.1	N21	119	 
256	1003	Vihtor Helen		0:46:51.6	N21	120	 
257	800	Nukk Tiina		0:46:53.9	N21	121	 
258	795	Mändmets Marjanne		0:47:24.6	N21	122	 
259	647	Isberg Anu		0:47:29.2	N40	53	 
260	933	Ungefug Jelena		0:48:33.8	N21	123	 
261	604	Arr Gerda	Helens OÜ	0:50:36.8	N21	124	 
262	727	Kägo Loore	EMT	0:50:40.3	N50	17	 
263	768	Maidre Helen	Elisa SK	0:50:50.0	N21	125	 
264	922	Tooming Sirli		0:51:04.9	N21	126	 
265	1009	Ellermann-Lill Piret		0:51:12.8	N40	54	 
266	573	Mägi Gerly		0:51:14.3	N18	7	 
267	932	Ummer Katri		0:51:58.0	N35	37	 
268	642	Hindrea Lily		0:52:11.9	N50	18	 
269	918	Tobro Ane		0:52:12.0	N60	6	 
270	582	Monahhova Larissa	JK START	0:52:28.1	N40	55	 
271	583	Baikova Anna	JK START	0:52:28.4	N50	19	 
272	788	Mugur Airi-Anu		0:54:47.4	N21	127	 
273	882	Sergo Tiina	Eesti Energia SK	0:54:57.9	N21	128	 
274	584	Hannus Kerli		0:55:50.3	N21	129	 
275	836	Põderson Kaisa	Vendor Eesti	0:58:19.2	N21	130	 
276	708	Kook Lilian	Vendor Eesti	0:58:20.0	N40	56	 
277	707	Kook Anna	Vendor Eesti	0:58:20.1	N21	131	 
278	934	Ungru Marianne	Vendor Eesti	0:58:21.6	N21	132	 
279	861	Ruiso Esther	Vendor Eesti	0:58:21.7	N21	133	 
280	585	Mauer Lidia		0:59:41.9	N60	7	 
281	596	Anni Kersti		1:00:05.7	N40	57	 
