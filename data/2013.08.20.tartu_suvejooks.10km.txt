Koht	Nkoht	Nr	Nimi	Klubi	Linn_Maak	Aeg	Kaotus	Vkl	Vkl_koht	NetoAeg
1	 	2	Dmitri Aristov	 	Ida-Virumaa	00:32:48	 	M	1	00:32:48
2	 	1	Vjatšeslav Košelev	KJK"Kalev" Sillamäe	Sillamäe	00:33:00	00:00:12	MV40	1	00:33:00
3	 	3	Hasso Paap	TÜ ASK	Tartu	00:34:20	00:01:32	M	2	00:34:20
4	 	6	Anti Puustus	Võnnu Spordiklubi	Võnnu vald	00:34:55	00:02:07	M	3	00:34:54
5	 	5	Deniss Košelev	TÜASK	 	00:35:26	00:02:38	M	4	00:35:24
6	 	388	Urmas Utar	 	Ülenurme	00:35:34	00:02:46	MV40	2	00:35:33
7	 	10	Olari Orm	 	Otepää	00:35:58	00:03:10	M	5	00:35:57
8	 	415	Mart Andresson	Alternatiivsed Jooksjad	Tartu	00:36:06	00:03:18	M	6	00:36:06
9	 	240	Kaarel Piip	 	 	00:37:36	00:04:48	M	7	00:37:35
10	 	4	Anton Salinin	 	 	00:38:27	00:05:39	MJ	1	00:38:26
11	 	384	Kaspar Songisepp	Elva Suusaklubi	 	00:38:34	00:05:46	M	8	00:38:34
12	 	48	Pärtel Piirimäe	 	Tartu	00:38:57	00:06:09	MV40	3	00:38:56
13	 	396	Joosep Mooses	OÜ Ravikäsi	Tartu	00:38:59	00:06:11	M	9	00:38:47
14	 	177	Raul Luik	 	Viljandi	00:39:11	00:06:23	M	10	00:39:09
15	 	313	Gaabriel Tavits	Weekend Warriors II	Tartu	00:39:15	00:06:27	MV40	4	00:39:08
16	 	377	Chris Poll	Estonian Ultrarunners Team	Kadrina	00:39:54	00:07:06	M	11	00:39:54
17	 	31	Viktor Lääts	 	 	00:40:02	00:07:14	MV60	1	00:39:59
18	 	182	Lauri Enn	 	Tartu	00:40:09	00:07:21	M	12	00:40:08
19	 	168	Mikk Medijainen	A. Le Coq	Tartu	00:40:15	00:07:27	M	13	00:40:11
20	 	307	Kaido Vahkal	FB Jooksmine	Tartu	00:40:21	00:07:33	M	14	00:40:19
21	 	96	Martin Kala	 	Tartu	00:40:23	00:07:35	M	15	00:40:21
22	 	408	Marek Salla	Türi Jõud	Türi	00:40:29	00:07:41	M	16	00:40:28
23	 	293	Sander Lepik	Evari Ehitus	Tartu	00:40:35	00:07:47	M	17	00:40:33
24	 	226	Erki Teder	 	Jõgeva	00:40:36	00:07:48	M	18	00:40:34
25	 	418	Sven Erik	 	 	00:40:38	00:07:50	MJ	2	00:40:35
26	 	433	Tiit Tali	 	 	00:40:38	00:07:50	MV40	5	00:40:38
27	 	230	Mihkel Gill	 	Tartu vald	00:40:39	00:07:51	M	19	00:40:37
28	 	423	Tiit Kibuspuu	VAK STAIER	 	00:40:40	00:07:52	MV40	6	00:40:38
29	1	356	Grete Tõnne	EMÜ SK	Põltsamaa	00:40:41	00:07:53	N	1	00:40:41
30	 	308	Karol Hanga	Kose Spordikool	 	00:40:43	00:07:55	MJ	3	00:40:36
31	 	214	Kajar Tilga	 	Valgamaa	00:40:47	00:07:59	MV40	7	00:40:45
32	 	123	Arne Türkson	Suusahullud	Tartu	00:40:49	00:08:01	MV40	8	00:40:47
33	2	361	Janelle Tarum	KF Suusaklubi	 	00:40:53	00:08:05	N	2	00:40:51
34	 	382	Mikk Põdra	 	Nõo	00:41:34	00:08:46	M	20	00:41:32
35	 	199	Heimar Pehk	 	Saverna	00:41:40	00:08:52	M	21	00:41:30
36	 	173	Raido Matson	 	Tartu	00:41:40	00:08:52	MV40	9	00:41:35
37	3	147	Kaisa Kukk	Jooksupartner	Tallinn	00:41:45	00:08:57	N	3	00:41:43
38	 	420	Priit Põlgast	 	 	00:42:13	00:09:25	M	22	00:42:10
39	 	379	Andre Lomaka	 	 	00:42:15	00:09:27	M	23	00:42:05
40	 	291	Rainer Villemson	 	 	00:42:16	00:09:28	M	24	00:42:16
41	 	372	Risto Ülem	KJK Saare	Muhu vald	00:42:28	00:09:40	M	25	00:42:19
42	 	329	Mario Laanemets	Kaitsevägi	 	00:42:28	00:09:40	M	26	00:42:17
43	 	222	Aleksandr Lissovski	 	Tartu	00:42:30	00:09:42	M	27	00:42:21
44	 	321	Raido Roben	Kaitsevägi	 	00:42:46	00:09:58	M	28	00:42:32
45	 	373	Oliver Kulbas	 	 	00:42:58	00:10:10	M	29	00:42:56
46	 	77	Tõnu Ilves	 	Tartu	00:43:02	00:10:14	MV40	10	00:42:58
47	 	172	Antti Nöps	Tööriistakeskus	Tartu	00:43:04	00:10:16	MV40	11	00:43:04
48	 	318	Kalmer Vaakmann	 	 	00:43:04	00:10:16	MV40	12	00:42:56
49	 	344	Joosep Mölder	 	 	00:43:06	00:10:18	M	30	00:42:59
50	 	351	Indrek Talpsep	 	 	00:43:07	00:10:19	M	31	00:43:01
51	 	171	Andres Nöps	TÜ ASK	Tartu	00:43:08	00:10:20	MJ	4	00:43:08
52	4	143	Sille Rell	 	Tartu	00:43:14	00:10:26	NV35	1	00:43:13
53	5	312	Esta Pilt	SA Tartu Sport	Tartu	00:43:43	00:10:55	NV35	2	00:43:41
54	 	237	Kaspar Kukk	FC Kuremaa	Jõgeva	00:43:43	00:10:55	M	32	00:43:34
55	 	319	Tarmo Tikk	CrossFit	Tartu	00:43:44	00:10:56	M	33	00:43:26
56	 	414	Taivo Pallotedder	TriSmile	Tartu vald	00:43:48	00:11:00	M	34	00:43:38
57	 	229	Jakob Gill	 	Tartu vald	00:43:50	00:11:02	MJ	5	00:43:49
58	6	397	Maris Tamm	TÜ ASK	Tartu	00:43:52	00:11:04	N	4	00:43:50
59	 	349	Robert Oetjen	 	Elva	00:43:57	00:11:09	MV40	13	00:43:52
60	 	392	Kristjan Lusikas	Mobi Solutions OÜ	Tartu	00:44:02	00:11:14	M	35	00:43:56
61	 	383	Ingmar Pärtelpoeg	 	 	00:44:10	00:11:22	MV40	14	00:43:59
62	 	129	Raul Kangur	Jooksuklubi Sarma	Saaremaa	00:44:13	00:11:25	MV40	15	00:44:13
63	 	135	Tanel Koho	 	 	00:44:16	00:11:28	M	36	00:44:11
64	7	33	Elin Ilves	 	Tartu	00:44:21	00:11:33	NV35	3	00:44:18
65	 	295	Ragnar Virma	 	Harjumaa	00:44:34	00:11:46	M	37	00:44:34
66	 	339	Raul Liebenau	 	 	00:44:35	00:11:47	M	38	00:44:26
67	 	159	Martin Liira	 	Tartumaa	00:44:36	00:11:48	M	39	00:44:30
68	 	202	Allan Mitt	 	Tartu	00:44:41	00:11:53	M	40	00:44:36
69	 	225	Ülar Lehiste	Eesti Energia Spordiklubi	Valgamaa	00:44:57	00:12:09	MV40	16	00:44:46
70	 	326	Andrei Smirnov	Kaitsevägi	 	00:45:10	00:12:22	M	41	00:44:58
71	 	378	Ain Kivisild	 	 	00:45:13	00:12:25	MV40	17	00:44:55
72	 	328	Vladimir Titov	Kaitsevägi	 	00:45:14	00:12:26	M	42	00:44:59
73	 	109	Ergo Meier	JK Hermes	 	00:45:25	00:12:37	MV40	18	00:45:23
74	 	302	Siim Siigur	 	Tartu	00:45:26	00:12:38	M	43	00:45:16
75	 	346	Oliver Hannus	42 koma	Tartu	00:45:26	00:12:38	M	44	00:45:20
76	 	286	Karl Arthur Strandberg	 	Tartu	00:45:27	00:12:39	M	45	00:45:01
77	 	236	Rauno Lõiv	 	Tartu	00:45:32	00:12:44	M	46	00:45:22
78	 	435	Andres Mäekivi	21CC Traitloniklubi	 	00:45:38	00:12:50	MV40	19	00:45:29
79	 	363	Ingemar Maasikmäe	Tartu Kõrgem Kunstikool	Tartu	00:45:42	00:12:54	M	47	00:45:35
80	 	255	Ain Lubi	CrossFit	Tartu	00:45:47	00:12:59	MV40	20	00:45:41
81	 	70	Peep Teppo	 	Tartu	00:45:47	00:12:59	M	48	00:45:44
82	8	341	Eveli Kurg	 	Tallinn	00:45:55	00:13:07	N	5	00:45:44
83	 	320	Raido Köster	Kaitsevägi	 	00:46:00	00:13:12	M	49	00:45:46
84	 	41	Toomas Gross	 	Tartu	00:46:04	00:13:16	MV40	21	00:46:00
85	9	9	Ksenia Savtšenko	KJK"Kalev" Sillamäe	Sillamäe	00:46:13	00:13:25	NJ	1	00:46:13
86	 	195	Alar Jaanson	Alvetra OÜ	Tartu	00:46:18	00:13:30	M	50	00:46:14
87	 	174	Rait Ermann	Rademar	Tallinn	00:46:23	00:13:35	M	51	00:46:11
88	 	194	Heigo Otsa	 	 	00:46:26	00:13:38	M	52	00:46:22
89	10	220	Karola Kivilo	 	Avinurme	00:46:30	00:13:42	NJ	2	00:46:25
90	 	347	Kristo Reitav	Popsport	 	00:46:37	00:13:49	M	53	00:46:25
91	11	97	Marika Koplimägi	FB Jooksmine	Tartu	00:46:39	00:13:51	NV35	4	00:46:36
92	12	239	Kerstin Uiboupin	OK Võru	Vastseliina	00:46:41	00:13:53	N	6	00:46:32
93	 	85	Ando Viispert	Ants Piibu bridžiklubi	Tartu	00:46:46	00:13:58	M	54	00:46:37
94	 	256	Alari Neithal	 	Haapsalu	00:46:49	00:14:01	M	55	00:46:40
95	 	114	Targo Tennisberg	 	Tartu	00:46:54	00:14:06	M	56	00:46:50
96	13	374	Airi Undrits	 	Tartu	00:46:54	00:14:06	N	7	00:46:49
97	 	386	Risto Kulbas	 	 	00:46:56	00:14:08	MV60	2	00:46:54
98	 	262	Hillar Irves	Ekstreempark	Võru	00:46:58	00:14:10	MV40	22	00:46:30
99	14	376	Ruth Kalda	TÜ ASK	Tartu	00:46:59	00:14:11	NV35	5	00:46:56
100	 	357	Indrek Mahla	Kaheksarattalised	Tartu	00:47:07	00:14:19	M	57	00:46:54
101	 	206	Rivo Varep	 	 	00:47:09	00:14:21	M	58	00:47:09
102	 	59	Maik Tukk	 	Tõrvandi	00:47:13	00:14:25	M	59	00:47:10
103	 	287	Jürgen Moor	 	 	00:47:16	00:14:28	M	60	00:47:11
104	 	325	Silver Korjus	Kaitsevägi	 	00:47:23	00:14:35	MJ	6	00:47:09
105	 	211	Arvi Kiik	 	Luunja vald	00:47:30	00:14:42	M	61	00:47:26
106	 	432	Margus Udeküll	 	 	00:47:35	00:14:47	M	62	00:47:34
107	 	178	Urmas Õunap	 	Viljandi	00:47:38	00:14:50	MV40	23	00:47:34
108	 	242	Jaanus Ruut	 	Viljandi	00:47:41	00:14:53	M	63	00:47:31
109	15	36	Järvi Valter	 	Tallinn	00:47:47	00:14:59	NV55	1	00:47:45
110	 	221	Helari Kaljuste	 	Tartu	00:47:48	00:15:00	M	64	00:47:40
111	 	422	Mati Merila	 	 	00:47:49	00:15:01	MV40	24	00:47:42
112	 	401	Mati Lohu	 	 	00:47:50	00:15:02	M	65	00:47:43
113	 	141	Keith Kukkela	 	Tartu	00:47:55	00:15:07	MJ	7	00:47:55
114	16	190	Mariann Sulg	OK Orvand	 	00:48:01	00:15:13	N	8	00:47:58
115	17	399	Kristin Männik	 	 	00:48:01	00:15:13	N	9	00:47:57
116	18	306	Jane Saluorg	 	 	00:48:02	00:15:14	N	10	00:47:56
117	 	218	Allar Erik	 	 	00:48:04	00:15:16	MJ	8	00:48:02
118	 	323	Sander Bubnov	Kaitsevägi	 	00:48:11	00:15:23	M	66	00:47:57
119	 	324	Kevin Starkopf	Kaitsevägi	 	00:48:11	00:15:23	MJ	9	00:48:10
120	 	338	Vladislav Tonkonogov	AS Hanza Tarkon	Tartu	00:48:15	00:15:27	MV60	3	00:48:04
121	 	28	Tiit Oinus	Melliste Racing Team	 	00:48:24	00:15:36	M	67	00:48:14
122	 	110	Raivo Meier	JK Hermes	Assaku	00:48:25	00:15:37	MV60	4	00:48:22
123	 	45	Aleksander Rajevski	 	Tartu	00:48:25	00:15:37	M	68	00:48:17
124	 	381	Jüri Laanmets	 	Tallinn	00:48:26	00:15:38	MV40	25	00:48:20
125	 	331	Gert Enno	Kaitsevägi	 	00:48:29	00:15:41	M	69	00:48:17
126	 	186	Harri Kankar	 	 	00:48:30	00:15:42	MV40	26	00:48:28
127	19	201	Minna Kuslap	 	Jõgeva	00:48:30	00:15:42	N	11	00:48:20
128	 	261	Alo Martinson	CCD COE	Tallinn	00:48:34	00:15:46	MV40	27	00:48:28
129	20	234	Andra Puusepp	 	 	00:48:39	00:15:51	NV35	6	00:48:38
130	 	245	Marko Perm	OÜ Vennad Ehitus	Tartu	00:48:40	00:15:52	MV40	28	00:48:38
131	 	332	Simo Lindmaa	Kaitsevägi	 	00:48:46	00:15:58	M	70	00:48:34
132	 	216	Taavi Kuha	OK Kape	 	00:48:50	00:16:02	M	71	00:48:35
133	 	431	Kristjan Kärner	Promens AS	 	00:48:52	00:16:04	M	72	00:48:46
134	21	23	Marge Türn	 	Tartu	00:49:00	00:16:12	NV35	7	00:48:53
135	 	228	Renee Praks	 	 	00:49:03	00:16:15	M	73	00:48:53
136	 	136	Priit Perv	 	Tartu	00:49:04	00:16:16	M	74	00:48:59
137	 	327	Taavi Ragul	Kaitsevägi	 	00:49:05	00:16:17	MJ	10	00:48:52
138	22	380	Ailen Mälgi	CrossFit	Tartu	00:49:07	00:16:19	N	12	00:48:58
139	23	35	Siiri Pilt	 	Tartu	00:49:09	00:16:21	NV35	8	00:49:07
140	 	88	Guido Bergmann	 	Tartu	00:49:12	00:16:24	MV40	29	00:49:07
141	 	118	Ermo Haas	 	Tartu	00:49:19	00:16:31	M	75	00:49:07
142	 	400	Kaido Maasen	 	Tartu	00:49:27	00:16:39	MV40	30	00:49:22
143	 	284	Siim Läänelaid	 	Ülenurme	00:49:27	00:16:39	M	76	00:49:19
144	 	183	Jaan Lehismets	Trimtex Baltic OÜ	Pärnumaa	00:49:30	00:16:42	M	77	00:49:24
145	 	276	Peeter Kurg	 	Tartu	00:49:39	00:16:51	M	78	00:49:35
146	24	91	Ingrid Ojang	Ants Piibu bridžiklubi	 	00:49:40	00:16:52	NV35	9	00:49:31
147	25	47	Tea Mey	 	Ülenurme	00:49:46	00:16:58	NV35	10	00:49:45
148	 	288	Kaido Sillar	 	Tartu	00:49:48	00:17:00	M	79	00:49:39
149	26	277	Egle Villik	 	Tartu	00:49:53	00:17:05	N	13	00:49:51
150	27	310	Kaidi Käämer	SA Tartu Sport	Tartu	00:49:58	00:17:10	NJ	3	00:49:48
151	 	269	Raivo Laanemets	 	Elva	00:50:00	00:17:12	M	80	00:49:54
152	28	268	Marii Ojastu	 	 	00:50:01	00:17:13	N	14	00:49:52
153	 	207	Risto Valdner	TÜ ASK	Tartu	00:50:11	00:17:23	MJ	11	00:50:06
154	29	79	Evelin Pellenen	 	Tartu	00:50:12	00:17:24	NV35	11	00:50:09
155	 	330	Sander Havi	Kaitsevägi	 	00:50:39	00:17:51	M	81	00:50:26
156	 	154	Aimar Kongi	Safran OÜ	Tartu	00:50:45	00:17:57	M	82	00:50:31
157	 	238	Antti Rammi	Ramest Ehitus Racing Team	 	00:50:47	00:17:59	M	83	00:50:38
158	30	66	Marilin Riives	Tartu SS Kalev	Tartu	00:50:48	00:18:00	NJ	4	00:50:47
159	 	42	Oskar Hint	 	 	00:51:01	00:18:13	M	84	00:50:48
160	 	337	Ranno Raudsik	Kaitsevägi	 	00:51:04	00:18:16	M	85	00:50:50
161	 	52	Tõnis Palgi	 	Tartu	00:51:12	00:18:24	M	86	00:50:57
162	31	74	Margit Kannel	 	 	00:51:13	00:18:25	N	15	00:51:07
163	32	279	Jaanika Kurgjärv	 	Tartu	00:51:22	00:18:34	N	16	00:51:15
164	 	30	David Arutyunyan	Tartu Ülikool	Tartu	00:51:25	00:18:37	M	87	00:51:11
165	 	106	Andrei Lopsik	Eesti Energia Spordiklubi	Kohtla-Järve	00:51:30	00:18:42	MV40	31	00:51:26
166	33	364	Laura Ernits	Tabasalu TK	 	00:51:31	00:18:43	NJ	5	00:51:28
167	 	365	Frederik Mathias Helm	 	 	00:51:33	00:18:45	MJ	12	00:51:29
168	 	231	Paul Gill	 	Tartu vald	00:51:34	00:18:46	MV40	32	00:51:32
169	34	340	Tiiu Müürsepp	Kevek	Tartu	00:51:40	00:18:52	NV55	2	00:51:35
170	 	410	Valdur Männiste	 	 	00:51:45	00:18:57	MV40	33	00:51:33
171	35	398	Made Vares	Suusahullud	Tartumaa	00:51:46	00:18:58	NV35	12	00:51:40
172	36	137	Jana Roosimägi	 	Tartu	00:51:55	00:19:07	N	17	00:51:52
173	37	197	Silja Laurits	 	 	00:51:57	00:19:09	NV35	13	00:51:55
174	 	334	Andrus Reinstein	 	Tartu	00:52:02	00:19:14	MV40	34	00:51:47
175	 	360	Mihkel Järviste	Tammeka	Tartu	00:52:12	00:19:24	MJ	13	00:52:06
176	38	111	Ingrit Ernits	 	Jõgeva	00:52:12	00:19:24	NV35	14	00:52:09
177	39	113	Katrin Gabrel	 	Tartu	00:52:16	00:19:28	NV35	15	00:52:11
178	40	27	Sigrid Raud	 	 	00:52:20	00:19:32	N	18	00:52:07
179	 	430	Aleksander Dubovik	 	 	00:52:22	00:19:34	MV40	35	00:52:20
180	 	322	Lenar Valk	Kaitsevägi	 	00:52:32	00:19:44	MJ	14	00:52:17
181	41	285	Maret Pihu	 	Tartu	00:52:41	00:19:53	NV35	16	00:52:25
182	42	428	Kaari Tilga	 	 	00:52:42	00:19:54	NJ	6	00:52:35
183	43	78	Kristi Teppo	 	Tartu	00:52:52	00:20:04	N	19	00:52:40
184	 	148	Mart Kelk	 	Tartu	00:52:53	00:20:05	M	88	00:52:40
185	44	243	Mari Hummal	 	Tartu	00:53:03	00:20:15	NV35	17	00:53:00
186	 	385	Erki Palm	 	Vastseliina	00:53:22	00:20:34	MV40	36	00:53:20
187	45	275	Maila Kuus	A. Le Coq	 	00:53:23	00:20:35	NV35	18	00:53:20
188	 	130	Mati Ruul	 	Tallinn	00:53:24	00:20:36	MV40	37	00:53:15
189	 	350	Valdek Rohtma	SA Tartu Sport	Haaslava vald	00:53:39	00:20:51	M	89	00:53:36
190	 	233	Eero-Margo Neeme	 	Tartu	00:53:44	00:20:56	M	90	00:53:42
191	46	294	Lille Välja	 	 	00:53:44	00:20:56	NJ	7	00:53:44
192	 	210	Joosep Kivastik	TÜ ASK	Tartu	00:53:46	00:20:58	M	91	00:53:32
193	 	254	Lauri Hein	 	Tartu	00:53:51	00:21:03	M	92	00:53:37
194	 	63	Martin Riives	Tartu SS Kalev	Tartu	00:53:58	00:21:10	MJ	15	00:53:56
195	47	298	Lilian Valge	 	Kadrina	00:54:02	00:21:14	NJ	8	00:54:01
196	 	68	Alvar Uus	 	 	00:54:02	00:21:14	M	93	00:53:54
197	48	407	Katre Lillo	 	Tartu	00:54:18	00:21:30	N	20	00:54:10
198	 	438	Valvo Piilberg	 	 	00:54:24	00:21:36	M	94	00:54:05
199	 	26	Ülo Luuka	JOTE	Tartu	00:54:37	00:21:49	MV40	38	00:54:26
200	49	84	Katrin Hämäläinen	 	Tallinn	00:54:37	00:21:49	N	21	00:54:34
201	 	270	Ivar Parmas	CrossFit	Tartu	00:54:48	00:22:00	M	95	00:54:42
202	50	150	Oksana Aasa	 	Võru	00:54:51	00:22:03	N	22	00:54:40
203	51	390	Kairit Fitzgerald	 	 	00:54:57	00:22:09	N	23	00:54:44
204	 	271	Robin Sova	 	 	00:54:59	00:22:11	MJ	16	00:54:55
205	52	64	Aire Riives	 	Tartu	00:55:09	00:22:21	NV35	19	00:55:05
206	53	395	Floor Vodde	 	Uhmardu	00:55:10	00:22:22	NV35	20	00:54:51
207	 	160	Margus Remmel	 	Tartu	00:55:17	00:22:29	M	96	00:55:09
208	54	156	Külli Kori	 	Tartu	00:55:22	00:22:34	N	24	00:55:15
209	 	402	Peep Purje	 	Tartu	00:55:25	00:22:37	M	97	00:55:13
210	 	278	Karli Ligi	 	 	00:55:25	00:22:37	M	98	00:55:12
211	 	427	Danver Hans Värv	 	 	00:55:29	00:22:41	MJ	17	00:55:22
212	 	60	Ilmar Tagel	TriSmile	Põlva	00:55:30	00:22:42	MV60	5	00:55:26
213	55	107	Vivika Peets	 	Tartumaa	00:55:35	00:22:47	NV35	21	00:55:30
214	56	426	Hüite Bergmann	 	 	00:55:35	00:22:47	NV35	22	00:55:30
215	57	67	Kätlin Daškova	Ramest Ehitus Racing Team	Tartu	00:55:42	00:22:54	NV35	23	00:55:32
216	 	304	Ken Viidebaum	JK Tammeka	Tartu	00:55:45	00:22:57	M	99	00:55:32
217	58	303	Kristiina Kurg	 	 	00:55:46	00:22:58	N	25	00:55:42
218	59	149	Marge Kann	 	Tartu	00:55:49	00:23:01	N	26	00:55:38
219	 	12	Marko Kass	 	Tartu	00:55:52	00:23:04	M	100	00:55:40
220	 	185	Gregor Randla	 	 	00:55:58	00:23:10	MJ	18	00:55:56
221	60	369	Kristiina Lemetti	 	Tartu	00:56:00	00:23:12	NJ	9	00:55:50
222	61	366	Merlin Rehema	SK Metsasõbrad	Tallinn	00:56:01	00:23:13	N	27	00:55:51
223	62	121	Elen Rüütel	 	Tartu	00:56:03	00:23:15	N	28	00:55:51
224	63	51	Kristi Kõivik	 	Tartu	00:56:07	00:23:19	N	29	00:55:58
225	 	142	Ando Sõrmus	 	Tartu	00:56:10	00:23:22	M	101	00:56:03
226	 	434	Taavi Liias	 	 	00:56:17	00:23:29	M	102	00:56:01
227	 	116	Erkki Metsa	 	Tartu	00:56:18	00:23:30	M	103	00:56:06
228	 	336	Tanel Aamer	 	Tartu	00:56:21	00:23:33	M	104	00:56:07
229	64	140	Tiina Säälik	 	Kassinurme	00:56:22	00:23:34	NV35	24	00:56:15
230	65	189	Kristina Kossinkova	 	Tartu	00:56:23	00:23:35	N	30	00:56:20
231	 	145	Ahto Jaska	Kaitseliit	Tartu	00:56:28	00:23:40	MV40	39	00:56:22
232	66	352	Kärt Laarman	 	 	00:56:33	00:23:45	NV35	25	00:56:20
233	 	353	Keio Laarman	 	 	00:56:34	00:23:46	M	105	00:56:20
234	 	180	Matti Silber	JK Hermes	Elva	00:56:40	00:23:52	MV60	6	00:56:30
235	67	105	Terje Kasesalu	 	 	00:56:41	00:23:53	NV35	26	00:56:32
236	68	375	Lilian Kadaja-Saarepuu	 	Tartu	00:56:44	00:23:56	NV35	27	00:56:40
237	 	99	Marek Oja	 	Tartu	00:56:44	00:23:56	M	106	00:56:37
238	69	354	Kai Kippasto	Fitlife	 	00:56:45	00:23:57	NV35	28	00:56:42
239	 	370	Margo Lemetti	 	Tartu	00:56:48	00:24:00	M	107	00:56:37
240	70	56	Maili Nurme	 	Tartu	00:56:51	00:24:03	N	31	00:56:44
241	71	57	Maia Boltovsky	FB jooksmine/KVÜÕA	 	00:56:55	00:24:07	NV35	29	00:56:51
242	72	90	Helle Hallik	Seiklusporr/ JOKA	Tartu	00:56:55	00:24:07	N	32	00:56:52
243	73	436	Kaija Paas	Crossfit Tartu	 	00:56:56	00:24:08	N	33	00:56:54
244	74	359	Triinu Tintso	OÜ Ravikäsi	 	00:56:59	00:24:11	N	34	00:56:47
245	75	18	Kätlin Jansons	 	 	00:57:12	00:24:24	N	35	00:57:00
246	76	273	Kati Metsla	 	 	00:57:17	00:24:29	N	36	00:57:15
247	77	92	Kaire Klaus	 	Võrumaa	00:57:28	00:24:40	NV35	30	00:57:19
248	 	413	Mait Lõhmus	 	 	00:57:29	00:24:41	M	108	00:57:12
249	78	391	Maive Vill	 	Valgamaa	00:57:30	00:24:42	N	37	00:57:19
250	 	260	Rainer Speek	AS Tiksoja puidugrupp	Tartu	00:57:33	00:24:45	M	109	00:57:14
251	 	411	Jaan Lusikas	Reginett	Tartu	00:57:35	00:24:47	MV40	40	00:57:28
252	79	138	Tiiu Järv	 	Tartu	00:57:48	00:25:00	NV35	31	00:57:44
253	80	257	Tiiu Jüriado	CrossFit	 	00:57:51	00:25:03	NV35	32	00:57:47
254	81	83	Ulvi Lond	 	Tartu	00:58:01	00:25:13	NV35	33	00:57:53
255	82	46	Kadri Kivistik	 	Tartu	00:58:05	00:25:17	NV55	3	00:58:00
256	83	416	Helena Lemmats	Salomon	Tartu	00:58:20	00:25:32	NJ	10	00:58:10
257	 	362	Toomas Jürgenstein	 	 	00:58:20	00:25:32	MV40	41	00:58:08
258	84	122	Pille Nurmis	Elion SK	Tallinn	00:58:23	00:25:35	NV35	34	00:58:17
259	85	281	Merlin Alabert	 	Pärnumaa	00:58:26	00:25:38	N	38	00:58:16