normalize.pace <- function(speed, sex, t) {
  if (length(speed) > 1) {
    unlist(lapply(1:length(speed), function(i, speed, sex, t) {
      normalize.pace(speed[i], sex[i], t)
      }, speed, sex, t))
  } else if (!is.na(speed) && !is.na(sex)) {
    if (substring(sex, 1, 1) == "M") {
      a <- c(0.049550, -0.722884, 2.581438)
    } else {
      a <- c(0.033750, -0.509125, 1.898859)  
    }
    
    loss <- sum(a * c(t^2, t, 1))
    speed / (1 - loss / 100)
  } else
    NA
}

normalize.pace(2.85, "M", 16.24)
normalize.pace(2.49, "F", 21.75)

normalize.pace(c(2.85, 2.49), c("M", "F"), 21.75)

normalize.pace(5.01002, "F", 24)

