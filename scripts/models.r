library(data.table)

model.french <- function(old.speed, old.dist, new.dist) {
  unlist(lapply(old.speed, function(old.speed, old.dist, new.dist) {
    old.dist / new.dist  * old.speed
  }, old.dist, new.dist))
}

model.french(c(2, 4), 10, 20)


model.dm <- function(old.speed, old.dist, new.dist) {

  get.diff.frac <- function(old.dist, new.dist) {
    result <- 1
    dist.min <- min(old.dist, new.dist)
    dist.max <- max(old.dist, new.dist)
    
    limits <- c(10000, 21097, 42195)
    loss.per.meter <- c(0.000012, 0.000004, 0.000005)
    
    for (i in 1:length(limits)) {
      if (dist.min < limits[i]) {
        if (dist.max > limits[i])
          result <- result * (1 + (limits[i] - dist.min) * loss.per.meter[i])
        else
          return (result * (1 + (dist.max - dist.min) * loss.per.meter[i]) - 1)
      }
    }
    
    print("Unsupported distance")
    return (NA)
  }
  
  unlist(lapply(old.speed, function(old.speed, old.dist, new.dist) {
    diff <- get.diff.frac(old.dist, new.dist)
    if (old.dist < new.dist)
      diff <- -diff
    return ((1 + diff) * old.speed)
  }, old.dist, new.dist))
}

# model.dm(c(2, 4), 8000, 12000)
# 
# get.diff.frac(6300, 10000)
# get.diff.frac(10000, 21097)
# get.diff.frac(21097, 42195)

# assumes df.max
model.knn <- function(old.speed, old.dist, new.dist, k = 16, outliers = 3) {
  
  limits <- c(6400, 10000, 13800, 21097, 42195)
  df.knn <- df.max[c(1, 2, 5, 7, 9, 11)]
  
  get.intermediate.speed <- function(dist, dist1, dist2) {
    speed1 <- df.knn[[paste("m", dist1, sep = "")]]
    speed2 <- df.knn[[paste("m", dist2, sep = "")]]
    return ((dist - dist1) / (dist2 - dist1) * (speed2 - speed1) + speed1)
  }
  
  get.col.indices <- function(dist) {
    i <- length(limits[limits <= dist]) + 1
    if (i > 1 && i <= length(limits) && dist != limits[i - 1])
      c(i - 1, i) + 1
    else
      min(max(i - 1, 1), length(limits)) + 1
  }
  
  get.speed <- function(dist) {
    i <- get.col.indices(dist) - 1
    if (length(i) == 2)
      get.intermediate.speed(dist, limits[i[1]], limits[i[2]])
    else
      df.knn[[paste("m", limits[i], sep = "")]]
  }
  
  find.closest.index <- function(speed, value) {
    left <- 1
    right <- length(speed)
    while (left < right) {
      mid <- (right - left) %/% 2 + left
      if (abs(speed[mid] - value) < 0.0001)
        return (mid)
      else if (speed[mid] < value)
        left <- mid + 1
      else
        right <- mid - 1
    }
    
    if (right < mid) {
     if (right <= 0) 
       return (mid)
     else 
       ifelse(abs(speed[mid] - value) < abs(speed[right] - value), mid, right)
    } else {# if (left > mid)
      if (left > length(speed))
        return (mid)
      else
        ifelse(abs(speed[mid] - value) < abs(speed[left] - value), mid, left)
    }
  }
  
  find.neighbor.indices <- function(speed, value, k) {
    i <- find.closest.index(speed, value)
    left <- i
    right <- i
    while (right - left + 1 < k && (left > 1 || right < length(speed))) {
      if (left == 1)
        right <- right + 1
      else if (right == length(speed))
        left <- left - 1
      else if (abs(speed[left - 1] - value) < abs(speed[right + 1] - value))
        left <- left - 1
      else
        right <- right + 1
    }
    left:right
  }
  
  cols <- unique(c(get.col.indices(old.dist), get.col.indices(new.dist)))
  
  for (i in c(1, cols))
    df.knn <- df.knn[!is.na(df.knn[i]),]
  
  from.speed <- get.speed(old.dist)
  df.knn <- df.knn[order(from.speed),]
  from.speed <- from.speed[order(from.speed)]
  to.speed <- get.speed(new.dist)
  
  unlist(lapply(old.speed, function(old.speed) {
    neighbors <- find.neighbor.indices(from.speed, old.speed, k)
    n.speeds <- to.speed[neighbors]
    n.speeds <- n.speeds[order(n.speeds)]
    n.speeds <- n.speeds[(outliers + 1):(k - outliers)]
    mean(n.speeds)
  }))
}

model.knn(4, 10000, 42195)
