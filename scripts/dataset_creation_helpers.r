reverse.name <- function(name) {
  #print(name)
  if (length(name) > 1) {
    unlist(lapply(name, reverse.name))
  } else {
    names <- unlist(strsplit(name, " "))
    paste(paste(names[2:length(names)], collapse = " "), names[1], collapse = " ")
  }
}

is.samename <- function(name1, name2) {
  name1 <- toupper(name1)
  name2 <- toupper(name2)
  return((name1 == name2) || (name1 == reverse.name(name2)) || (reverse.name(name1) == name2))
}  

get.avg.speed <- function(seconds, distance) {
  1000 * distance / seconds
}

get.times <- function(racers, race) {
  vapply(racers, function(name) {
    index <- which(race$Nimi == name)
    if (length(index) == 0)
      return (NA)
    if (length(index) > 1) {
      print(name)
      print(index)
    }
    return (race$seconds[index])
  }, FUN.VALUE = 0, USE.NAMES = FALSE)
}

get.avg.speeds <- function(racers, race) {
  vapply(racers, function(name) {
    index <- which(race$Nimi == name)
    if (length(index) == 0)
      return (NA)
    if (length(index) > 1) {
      print(name)
      print(index)
    }
    return (race$avg.speed[index])
  }, FUN.VALUE = 0, USE.NAMES = FALSE)
}

get.duplicate.names <- function(df) {
  df$Nimi[duplicated(df$Nimi)]
}

get.sex <- function(race) {
  if ("Nkoht" %in% colnames(race))
    ifelse(grepl("\\d", race$Nkoht), "F", "M")
  else if ("Vkl" %in% colnames(race))
    ifelse(substring(race$Vkl, 1, 1) == "N", "F", "M")
  else
    print("No column to verify sex")
}
