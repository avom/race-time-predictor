library(hydroGOF)

error.single <- function(old.speed, old.dist, new.speed, new.dist, FUN) {
  model.speed <- FUN(old.speed, old.dist, new.dist)
  rmse <- rmse(model.speed, new.speed)
  percent <- 100 * mean(abs(model.speed / new.speed - 1))
  c(rmse, percent)
}

df.9K.42K <- df[!is.na(df$kahesilla_8.9km) & !is.na(df$tallinna_sygisjooks_42.2km),]
df.9K.42K <- df[!is.na(df$) & !is.na(df$tallinna_sygisjooks_42.2km),]

#nrow(df.9K.42K)
#head(df)

error.single(df.9K.42K$kahesilla_8.9km, 8900, df.9K.42K$tallinna_sygisjooks_42.2km, 42195, model.french)
error.single(df.9K.42K$kahesilla_8.9km, 8900, df.9K.42K$tallinna_sygisjooks_42.2km, 42195, model.vo2max)
error.single(df.9K.42K$kahesilla_8.9km, 8900, df.9K.42K$tallinna_sygisjooks_42.2km, 42195, model.dm)
error.single(df.9K.42K$kahesilla_8.9km, 8900, df.9K.42K$tallinna_sygisjooks_42.2km, 42195, model.knn)
error.single(df.9K.42K$kahesilla_8.9km, 8900, df.9K.42K$tallinna_sygisjooks_42.2km, 42195, model.Cameron)


error.single(df.9K.42K$kahesilla_8.9km, 8900, df.9K.42K$tallinna_sygisjooks_42.2km, 42195, model.french)

model.dm(df.9K.42K$kahesilla_8.9km, 8900, 42195) 

df.9K.42K$kahesilla_8.9km

df.13K.42K <- df[!is.na(df$rahvajooks_13.6km) & !is.na(df$tallinna_sygisjooks_42.2km),]

error.single(df.13K.42K$rahvajooks_13.6km, 13600, df.13K.42K$tallinna_sygisjooks_42.2km, 42195, model.vo2max)
error.single(df.13K.42K$rahvajooks_13.6km, 13600, df.13K.42K$tallinna_sygisjooks_42.2km, 42195, model.dm)
error.single(df.13K.42K$rahvajooks_13.6km, 13600, df.13K.42K$tallinna_sygisjooks_42.2km, 42195, model.knn)
error.single(df.13K.42K$rahvajooks_13.6km, 13600, df.13K.42K$tallinna_sygisjooks_42.2km, 42195, model.Cameron)
