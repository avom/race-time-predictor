# setwd("")


factorToSeconds <- function(x) {
  if (!is.character(x)) stop("x must be a character string of the form H:M:S")
  if (length(x)<=0)return(x)
  
  unlist(
    lapply(x,
           function(i){
             i <- as.numeric(strsplit(i,':',fixed=TRUE)[[1]])
             if (length(i) == 3) 
               i[1]*3600 + i[2]*60 + i[3]
             else if (length(i) == 2) 
               i[1]*60 + i[2]
             else if (length(i) == 1) 
               i[1]
             else
               NA
           }  
    )  
  )  
}

filename <- "2014.09.14.tallinna_maraton.42.2km.txt"
#warnings()

read.results <- function(filename) {
  result <- read.table(filename, sep = "\t", header = TRUE, encoding = "UTF-8")
  result$seconds <- factorToSeconds(as.character(result$Aeg))
  subset(result, !is.na(seconds))
}

clean.results <- function(df) {
  df[!(duplicated(df$Nimi) | duplicated(df$Nimi, fromLast = TRUE)), ]
  df$Nimi <- toupper(as.character(df$Nimi))
  df
}

