filenames.2010 <- c(
  "2010.04.24.korvemaa_kevadjooks.16km.txt", 
  NA, # "2010.04.24.korvemaa_kevadjooks.6.3km.txt", 
  "2010.05.09.tartu_jooksumaraton.23.4km.txt",
  "2010.06.05.tallinna_olympiajooks.7.2km.txt",
  NA,
  NA,
  NA,
  "2010.06.12.harku_jarvejooks.6.4km.txt",
  "2010.07.10.pyhajarve_jooks.10.7km.txt",
  NA,
  NA,
  "2010.08.28.ylemiste_jarvejooks.5km.txt",
  "2010.08.28.ylemiste_jarvejooks.14km.txt",
  "2010.09.05.kahe_silla_jooks.9km.txt",
  "2010.09.12.tallinna_maraton.10km.txt",
  NA,
  "2010.09.12.tallinna_maraton.42km.txt",
  "2010.10.03.rahvajooks.13.6km.txt",
  "2010.10.03.rahvajooks.6.5km.txt"
)

filenames.2011 <- c(
  "2011.04.23.korvemaa_kevadjooks.16km.txt", 
  "2011.04.23.korvemaa_kevadjooks.6.3km.txt", 
  "2011.05.08.tartu_jooksumaraton.23.4km.txt",
  "2011.06.04.tallinna_olympiajooks.7.2km.txt",
  NA,
  NA,
  NA,
  "2011.06.11.harku_jarvejooks.6.3km.txt",
  "2011.07.09.pyhajarve_jooks.10.7km.txt",
  "2011.08.13.narva_energiajooks.21.1km.txt",
  "2011.08.13.narva_energiajooks.7km.txt",
  "2011.08.27.ylemiste_jarvejooks.5km.txt",
  "2011.08.27.ylemiste_jarvejooks.14km.txt",
  "2011.09.04.kahe_silla_jooks.9km.txt",
  "2011.09.11.tallinna_sygisjooks.10km.txt",
  "2011.09.11.tallinna_sygisjooks.21.1km.txt",
  "2011.09.11.tallinna_sygisjooks.42.2km.txt",
  "2011.10.02.rahvajooks.13.6km.txt",
  "2011.10.02.rahvajooks.6.5km.txt"
)

filenames.2012 <- c(
  "2012.04.28.korvemaa_kevadjooks.16km.txt", 
  "2012.04.28.korvemaa_kevadjooks.6.3km.txt", 
  "2012.05.13.tartu_jooksumaraton.23.4km.txt",
  NA,
  "2012.06.02.tartu_kevadjooks.10km.txt",
  "2012.06.08.oojooks.10km.txt",
  "2012.06.08.oojooks.21.1km.txt",
  "2012.06.16.harku_jarvejooks.6.3km.txt",
  "2012.07.07.pyhajarve_jooks.10.7km.txt",
  "2012.08.11.narva_energiajooks.21.1km.txt",
  "2012.08.11.narva_energiajooks.7km.txt",
  "2012.08.25.ylemiste_jarvejooks.5km.txt",
  "2012.08.25.ylemiste_jarvejooks.14km.txt",
  "2012.09.02.kahe_silla_jooks.8.9km.txt",
  "2012.09.09.tallinna_sygisjooks.10km.txt",
  "2012.09.09.tallinna_sygisjooks.21.1km.txt",
  "2012.09.09.tallinna_sygisjooks.42.2km.txt",
  "2012.10.06.rahvajooks.13.6km.txt",
  "2012.10.06.rahvajooks.6.5km.txt"
)

race.names <- c(
  "korvemaa", 
  "korvemaa", 
  "otepaa", 
  "olympia",
  "olympia",
  "oojooks",
  "oojooks",
  "harku", 
  "pyhajarve", 
  "narva", 
  "narva", 
  "ylemiste", 
  "ylemiste", 
  "kahesilla", 
  "tallinna_sygisjooks", 
  "tallinna_sygisjooks", 
  "tallinna_sygisjooks", 
  "rahvajooks", 
  "rahvajooks"
)

distances.2010 <-         c(16, 6.3, 23.4, 7.2, 10, 10, 21.1, 6.4, 10.7, 21.1,  7,  5, 14,  9,   10, 21.1, 42.2, 13.6, 6.5)
distances.2011 <-         c(16, 6.3, 23.4, 7.2, 10, 10, 21.1, 6.3, 10.7, 21.1,  7,  5, 14,  9,   10, 21.1, 42.2, 13.6, 6.5)
distances.2012 <-         c(16, 6.3, 23.4, 7.2, 10, 10, 21.1, 6.3, 10.7, 21.1,  7,  5, 14,  8.9, 10, 21.1, 42.2, 13.6, 6.5)

first.names.first.2010 <- c(F,    F,    T,   F,  F,    F,  F,   F,    T,    F,  F,  T,  F,  F,    F,    F,    F,  T,   T)
first.names.first.2011 <- c(F,    F,    T,   F,  F,    F,  F,   F,    F,    F,  F,  F,  F,  F,    F,    F,    F,  F,   T)
first.names.first.2012 <- c(F,    F,    T,   F,  F,    F,  F,   F,    F,    F,  F,  F,  F,  T,    F,    F,    F,  F,   F)

temps.2010 <-             c( 7,   7,    9,  13, NA,   NA, NA,  14,   25,   NA, NA, 14, 14, 14,   17,   NA,   16, 13,  13)
temps.2011 <-             c(13,  13,   17,  20, NA,   23, 23,  24,   27,   12, 9,  24, 24, 20,   17,   NA,   15, 13,  13)
temps.2012 <-             c(15,  15,    8,  NA, 12,   11, 11,  19,   23,   14, 14, 17, 17, 19,   13,   NA,   15, 12,  13)

# df.2010.orig <- create.race.data.frame(filenames.2010, race.names, distances.2010, first.names.first.2010, NA, 2010)
# df.2011.orig <- create.race.data.frame(filenames.2011, race.names, distances.2011, first.names.first.2011, NA, 2011)
# df.2012.orig <- create.race.data.frame(filenames.2012, race.names, distances.2011, first.names.first.2012, NA, 2011)

df.2010 <- create.race.data.frame(filenames.2010, race.names, distances.2010, first.names.first.2010, temps.2010, 2010)
df.2011 <- create.race.data.frame(filenames.2011, race.names, distances.2011, first.names.first.2011, temps.2011, 2011)
df.2012 <- create.race.data.frame(filenames.2012, race.names, distances.2012, first.names.first.2012, temps.2012, 2012)

head(df.2010)
head(df.2011)
head(df.2012)
# head(df.2010.orig)
# head(df.2011.orig)
# head(df.2012.orig)

# write.table(df.2010.orig, "df.2010.orig.txt", sep = "\t")
# write.table(df.2011.orig, "df.2011.orig.txt", sep = "\t")
# write.table(df.2012.orig, "df.2012.orig.txt", sep = "\t")
write.table(df.2010, "df.2010.txt", sep = "\t")
write.table(df.2011, "df.2011.txt", sep = "\t")
write.table(df.2012, "df.2012.txt", sep = "\t")

names(df.2010)[9] <- "harku_6.3km"
names(df.2011)[9] <- "harku_6.3km"
names(df.2012)[9] <- "harku_6.3km"
names(df.2010)[15] <- "kahesilla_8.9km"
names(df.2011)[15] <- "kahesilla_8.9km"
names(df.2012)[15] <- "kahesilla_8.9km"

df <- rbind(df.2010, df.2011, df.2012)
head(df)

write.table(df, "df.txt", sep = "\t")

df.sub <- df[rowSums(!is.na(df)) > 2,]

write.table(df.sub, "df_sub.txt", sep = "\t")

