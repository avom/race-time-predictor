filenames.2013 <- c(
  "2013.04.27.k6rvemaa.16km.txt",    
  "2013.05.12.tartu_jooksumaraton.10km.txt",
  "2013.05.12.tartu_jooksumaraton.23.4km.txt",
  "2013.05.25.tartu_olympiajooks.10km.txt",
  "2013.07.06.pyhaj2rve_jooks.10.7km.txt",
  "2013.08.10.narva_energiajooks.7km.txt",
  "2013.08.10.narva_energiajooks.21.1km.txt",
  "2013.08.20.tartu_suvejooks.10km.txt",
  "2013.08.24.ylemiste_j2rvejooks.14km.txt",
  "2013.09.01.kahe_silla_jooks.8.9km.txt",
  "2013.09.08.tallinna_maraton.42.2km.txt",
  "2013.10.05.tartu_linnamaraton.21.1km.txt",
  "2013.10.05.tartu_linnamaraton.42.2km.txt",
  "2013.10.06.rahvajooks.13.6km.txt",
  "2013.11.23.tartu_novembrijooks.5km.txt"
)

filenames.2014 <- c(
  "2014.04.27.k6rvemaa.16km.txt",    
  "2014.05.11.tartu_jooksumaraton.10km.txt",
  "2014.05.11.tartu_jooksumaraton.23.4km.txt",
  "2014.05.24.tartu_olympiajooks.10km.txt",
  "2014.07.05.pyhaj2rve_jooks.10.7km.txt",
  "2014.06.15.narva_energiajooks.7.km.txt",
  "2014.06.15.narva_energiajooks.21.1km.txt",
  "2014.08.20.tartu_suvejooks.10km.txt",
  "2014.08.30.ylemiste_j2rvejooks.14km.txt",
  "2014.09.07.kahe_silla_jooks.9.86km.txt",
  "2014.09.14.tallinna_maraton.42.2km.txt",
  "2014.10.04.tartu_linnamaraton.21.1km.txt",
  "2014.10.04.tartu_linnamaraton.42.2km.txt",
  "2014.10.05.rahvajooks.13.6km.txt",
  "2014.11.22.tartu_novembrijooks.5km.txt"  
)


race.names <- c(
  "korvemaa",
  "tartu_jooksumaraton",
  "tartu_jooksumaraton",
  "olympia",
  "pyhajarve", 
  "narva", 
  "narva", 
  "tartu_suvejooks",
  "ylemiste", 
  "kahesilla",
  "tallinna_maraton", 
  "tartu_linnamaraton",
  "tartu_linnamaraton",
  "rahvajooks",
  "novembrijooks"
)


distances.2013 <-         c(16, 10, 23.4, 10, 10.7, 7, 21.1, 10, 14, 8.9 , 42.2, 21.1, 42.2, 13.6, 5)
distances.2014 <-         c(16, 10, 23.4, 10, 10.7, 7, 21.1, 10, 14, 9.86, 42.2, 21.1, 42.2, 13.6, 5)

first.names.first.2013 <- c(F,   T,    T,  F,   F,  F,    F,  T,   F,   T,    F,    T,    T,    F, T)
first.names.first.2014 <- c(F,   T,    T,  T,   T,  T,    T,  T,   T,   T,    F,    T,    T,    F, T)

temps.2013 <-             c(5,  14,   14, 12,  22, 21,   21, 20,  17,  20,   19,    8,   11,   12, 6)
temps.2014 <-             c(18, 11,   11, 28,  20, 12,   12, 19,  15,  22,   16,   11,   12,    9, 1)


df.2013 <- create.race.data.frame(filenames.2013, race.names, distances.2013, first.names.first.2013, temps.2013, 2013)
df.2014 <- create.race.data.frame(filenames.2014, race.names, distances.2014, first.names.first.2014, temps.2014, 2014)

head(df.2013)
head(df.2014)

write.table(df.2013, "df.2013.txt", sep = "\t")
write.table(df.2014, "df.2014.txt", sep = "\t")

df.2013$kahesilla_9.86km <- rep(NA, nrow(df.2013))
df.2014$kahesilla_8.9km <- rep(NA, nrow(df.2014))

df.test <- rbind(df.2013, df.2014)
head(df.test)

write.table(df.test, "df.test.txt", sep = "\t")

#df.sub <- df[rowSums(!is.na(df)) > 3,]

#write.table(df.sub, "df_sub.txt", sep = "\t")

