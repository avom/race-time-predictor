time.diffs <- function(old.speed, old.dist, new.speed, new.dist, FUN) {
  model.speed <- FUN(old.speed, old.dist, new.dist)
  abs(model.speed / new.speed - 1)
}


#df.test.pair <- df.test[!is.na(df.test$kahesilla_8.9km) & !is.na(df.test$tallinna_maraton_42.2km),]

#df.races <- data.frame(base = df.test.pair$kahesilla_8.9km, predict = df.test.pair$tallinna_maraton_42.2km)

# dist.base <- 8900
# dist.predict <- 42195
# 
# base.bot.limit <- 3.5189504962
# predict.bot.limit <- 2.7451044174
# 
# df.test.pair <- data.frame(base = df.test$kahesilla_8.9km, predict = df.test$tallinna_maraton_42.2km)

dist.base <- 42195
dist.predict <- 13600

base.bot.limit <- 2.7451044174
predict.bot.limit <- 3.2195750161

df.test.pair <- data.frame(base = df.test$tallinna_maraton_42.2km, predict = df.test$rahvajooks)

df.races <- df.test.pair[!is.na(df.test.pair$base) & !is.na(df.test.pair$predict),]

nrow(df.races)

errors.vo2max <- time.diffs(df.races$base, dist.base, df.races$predict, dist.predict, model.vo2max)
errors.Cameron <- time.diffs(df.races$base, dist.base, df.races$predict, dist.predict, model.Cameron)

errors.knn <- time.diffs(df.races$base, dist.base, df.races$predict, dist.predict, model.knn)
errors.dm <- time.diffs(df.races$base, dist.base, df.races$predict, dist.predict, model.dm)

mean(errors.vo2max)
mean(errors.iaaf)
mean(errors.Cameron)
mean(errors.knn)
mean(errors.dm)


t.test(errors.vo2max, errors.knn, paired = TRUE, alternative = "greater")
t.test(errors.iaaf, errors.knn, paired = TRUE, alternative = "less")
t.test(errors.iaaf, errors.knn, paired = TRUE, alternative = "greater")
t.test(errors.Cameron, errors.knn, paired = TRUE, alternative = "greater")

t.test(errors.vo2max, errors.dm, paired = TRUE, alternative = "greater")
t.test(errors.iaaf, errors.dm, paired = TRUE, alternative = "less")
t.test(errors.Cameron, errors.dm, paired = TRUE, alternative = "greater")
t.test(errors.Cameron, errors.dm, paired = TRUE, alternative = "less")

t.test(errors.dm, errors.knn, paired = TRUE, alternative = "greater")


top.base.speed <- unname(quantile(df.races$base, seq(0, 1, 0.025))[37])
top.predict.speed <- unname(quantile(df.races$predict, seq(0, 1, 0.025))[37])



df.notop <- df.races[df.races$base < top.base.speed & df.races$predict < top.predict.speed,]

nrow(df.notop)

errors.vo2max <- time.diffs(df.notop$base, dist.base, df.notop$predict, dist.predict, model.vo2max)
errors.Cameron <- time.diffs(df.notop$base, dist.base, df.notop$predict, dist.predict, model.Cameron)

errors.knn <- time.diffs(df.notop$base, dist.base, df.notop$predict, dist.predict, model.knn)
errors.dm <- time.diffs(df.notop$base, dist.base, df.notop$predict, dist.predict, model.dm)




df.nobot <- df.races[df.races$base > base.bot.limit & df.races$predict > predict.bot.limit,]

nrow(df.nobot)

errors.vo2max <- time.diffs(df.nobot$base, dist.base, df.nobot$predict, dist.predict, model.vo2max)
errors.Cameron <- time.diffs(df.nobot$base, dist.base, df.nobot$predict, dist.predict, model.Cameron)
errors.iaaf <- time.diffs(df.nobot$base, dist.base, df.nobot$predict, dist.predict, model.iaaf)

errors.knn <- time.diffs(df.nobot$base, dist.base, df.nobot$predict, dist.predict, model.knn)
errors.dm <- time.diffs(df.nobot$base, dist.base, df.nobot$predict, dist.predict, model.dm)




df.notopbot <- df.nobot[df.nobot$base < top.base.speed & df.nobot$predict < top.predict.speed,]
nrow(df.notopbot)

errors.vo2max <- time.diffs(df.notopbot$base, dist.base, df.notopbot$predict, dist.predict, model.vo2max)
errors.Cameron <- time.diffs(df.notopbot$base, dist.base, df.notopbot$predict, dist.predict, model.Cameron)
errors.iaaf <- time.diffs(df.notopbot$base, dist.base, df.notopbot$predict, dist.predict, model.iaaf)

errors.knn <- time.diffs(df.notopbot$base, dist.base, df.notopbot$predict, dist.predict, model.knn)
errors.dm <- time.diffs(df.notopbot$base, dist.base, df.notopbot$predict, dist.predict, model.dm)
