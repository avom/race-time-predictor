
source("d:\\DropBox\\School\\DataMining\\project\\src\\scripts\\data_loader.r")


model.Cameron <- function(old.speed, old.dist, new.dist){
  if (length(old.speed) > 1) {
    
    unlist(lapply(old.speed, model.Cameron, old.dist, new.dist))
  
  } else {
    
    old.timeS <- old.dist / old.speed 
    a <- 13.49681 - (0.000030363 * old.dist) + (835.7114 / old.dist ^ 0.7905)
    b <- 13.49681 - (0.000030363 * new.dist) + (835.7114 / new.dist ^ 0.7905)
    
    multiplier <- a / b
    
    new.timeS <- (old.timeS / old.dist) * multiplier * new.dist
    new.timeS / 3600
  }
}

#model.Cameron(c(5.02, 4.5), 16000, 23400)


getVo2Max <- function(d, t) {
  v <- d / t
  voMaxUp <- (0.000104 * (v ^ 2) + 0.182258 * v - 4.6)
  voMaxDown <- 0.2989558 * exp(-0.1932605 * t) + 0.1894393 * exp(-0.012778 * t) + 0.8
  voMaxUp / voMaxDown
}


model.vo2max <- function(old.speed, old.dist, new.dist) {
  if (length(old.speed) > 1)
    unlist(lapply(old.speed, model.vo2max, old.dist, new.dist))
  else {
    old.time <- old.dist / old.speed / 60
    old.vo2max <- getVo2Max(old.dist, old.time)
    left.min <- 1
    right.min <- 8 * 60
    while (TRUE) {
      mid.min <- (right.min - left.min) / 2 + left.min
      mid.vo2max <- getVo2Max(new.dist, mid.min)
#       print(paste("vo2max", mid.vo2max))
      if (abs(mid.vo2max - old.vo2max) < 0.01)
        return (new.dist / mid.min / 60)
      else if (mid.vo2max > old.vo2max)
        left.min <- mid.min
      else
        right.min <- mid.min        
    }
  }
}


model.iaaf <- function(old.speed, old.dist, new.dist) {
  df.iaaf <- read.table("iaaf_speed_table.txt", sep = "\t", header = TRUE, encoding = "UTF-8")
  limits <- c(5000, 10000, 15000, 16093, 20000, 25000, 30000, 42195)
  
  get.intermediate.speed <- function(dist, dist1, dist2) {
    speed1 <- df.iaaf[[paste("m", dist1, sep = "")]]
    speed2 <- df.iaaf[[paste("m", dist2, sep = "")]]
    return ((dist - dist1) / (dist2 - dist1) * (speed2 - speed1) + speed1)
  }
  
  get.col.indices <- function(dist) {
    i <- length(limits[limits <= dist]) + 1
    if (i > 1 && i <= length(limits) && dist != limits[i - 1])
      c(i - 1, i) + 1
    else
      min(max(i - 1, 1), length(limits)) + 1
  }
  
  get.speed <- function(dist) {
    i <- get.col.indices(dist) - 1
    if (length(i) == 2)
      get.intermediate.speed(dist, limits[i[1]], limits[i[2]])
    else
      df.iaaf[[paste("m", limits[i], sep = "")]]
  }
  
  find.closest.index <- function(speed, value) {
    left <- 1
    right <- length(speed)
    while (left < right) {
      mid <- (right - left) %/% 2 + left
      if (abs(speed[mid] - value) < 0.0001)
        return (mid)
      else if (speed[mid] < value)
        left <- mid + 1
      else
        right <- mid - 1
    }
    
    if (right < mid) {
      if (right <= 0) 
        return (mid)
      else 
        ifelse(abs(speed[mid] - value) < abs(speed[right] - value), mid, right)
    } else {# if (left > mid)
      if (left > length(speed))
        return (mid)
      else
        ifelse(abs(speed[mid] - value) < abs(speed[left] - value), mid, left)
    }
  }
  
  find.neighbor.indices <- function(speed, value, k) {
    i <- find.closest.index(speed, value)
    left <- i
    right <- i
    while (right - left + 1 < k && (left > 1 || right < length(speed))) {
      if (left == 1)
        right <- right + 1
      else if (right == length(speed))
        left <- left - 1
      else if (abs(speed[left - 1] - value) < abs(speed[right + 1] - value))
        left <- left - 1
      else
        right <- right + 1
    }
    left:right
  }
  
  cols <- unique(c(get.col.indices(old.dist), get.col.indices(new.dist)))
  
  for (i in c(1, cols))
    df.iaaf <- df.iaaf[!is.na(df.iaaf[i]),]
  
  from.speed <- get.speed(old.dist)
  to.speed <- get.speed(new.dist)
  
  unlist(lapply(old.speed, function(old.speed) {
    neighbor <- find.closest.index(from.speed, old.speed)
    if (abs(from.speed[neighbor] - old.speed) < 0.0001)
      return (to.speed[neighbor])
    else {
      neighbors <- find.neighbor.indices(from.speed, old.speed, 2)
      low <- neighbors[1]
      high <- neighbors[2]
      
      score.frac <- (old.speed - from.speed[low]) / (from.speed[high] - from.speed[low])
      
      low.speed <- to.speed[low]
      high.speed <- to.speed[high]
      
      return (score.frac * (high.speed - low.speed) + low.speed)
    }
  }))
}

#model.iaaf(3.51895, 5000, 10000)
#model.iaaf(c(3.51895, 3.5384, 3.7824726257, 3.5286804428), 5000, 42195)


model.iaaf2 <- function(old.speed, old.dist, new.dist) {
  datafile <- "d:\\DropBox\\School\\DataMining\\project\\src\\articles\\iaaf_speed_table.txt"
  ds <- read.csv(file=datafile,sep="\t",head=TRUE,stringsAsFactors = FALSE)
  head(ds)
  
  dRange <- getDistRange(old.dist)
  x1 <- dRange[1]
  x2 <- dRange[2]
  col1 <- dRange[3]
  col2 <- dRange[4]
  
  oldDistPerc <- getDistancePercentage(oldDist, x1, x2)
    
  result_pos <- ds[ getSpeedDif(ds[,col1], ds[,col2], oldDistPerc, old.speed) < 0.005,]  
  if (length(result_pos < 1){
    result_pos <- ds[ getSpeedDif(ds[,col1], ds[,col2], oldDistPerc, old.speed) < 0.01,]  
  }
  
  # get new time,linear decrease between iaaf times
  if (length(result_pos > 0){
    dRange <- getDistRange(new.dist)
    x1 <- dRange[1]
    x2 <- dRange[2]
    col1 <- dRange[3]
    col2 <- dRange[4]
    
    newDistPerc <- getDistancePercentage(new.dist, x1, x2)
    v1 <- result_pos[1,col1]
    v2 <- result_pos[1,col2]
    speedChange <- getSpeedDecreaseByPercent(v1, v2, newDistPerc)
    newSpeed  <- v1 - speedChange
    new.time <- new.dist / newSpeed
  }
}



getSpeedDecreaseByPercent <-  function(v1, v2, dPer){
  derc <- ((v1 - v2) * dPer / 100)
  derc
}

getSpeedDiff <- function(v1, v2, dPer, old.speed){
  h1 <- getSpeedDecreaseByPercent(v1, v2, dPer)
  diff <- abs(v1 - h1 - old.speed)
  #print(paste(v1, v2, h1, diff,  sep="   "))
  diff
}

getDistancePercentage <- function(oldDist, range1, range2){
  distPerc <- ((oldDist - range1) * 100) / (range2 - range1)
  distPerc
}

x1 <- 10000
x2 <- 15000
col1 <- "X10km"
col2 <- "X15km"
oldDist <- 10000

distPerc <- getDistancePercentage(oldDist, x1, x2)

result_pos <- ds[ getSpeedDiff(ds[,col1], ds[,col2], distPerc, 4.2) < 0.01,]
result_pos <- ds[ getSpeedDiff(ds[,col1], ds[,col2], 0, 3.4211426616) < 0.005,]

result_pos[1,col1]

getSpeedDiff(3.4153005464, 3.3178500332, 0, 3.4211426616)

temp <- "X5km"
head(ds[,temp])

getDistRange <- function(old.dist){
  if (old.dist >= 5000 && old.dist < 10000) range <- c(5000, 10000)
  if (old.dist >= 10000 && old.dist < 15000) range <- c(10000, 15000)
  if (old.dist >= 15000 && old.dist < 16093) range <- c(15000, 16093)
  if (old.dist >= 16093 && old.dist < 20000) range <- c(16093, 20000)
  if (old.dist >= 20000 && old.dist < 21097) range <- c(20000, 21097)
  if (old.dist >= 21097 && old.dist < 25000) range <- c(21097, 25000)
  if (old.dist >= 25000 && old.dist < 30000) range <- c(25000, 30000)
  if (old.dist >= 30000 && old.dist < 42195) range <- c(21097, 42195)
}

#getVo2Max(16000, 53)
#predTime <- getPrediction_Vo2Max(23400, "01:18:36", 6400)
#predTime <- getPrediction_Vo2Max(6400, "00:27:03", 21100)
model.vo2max(6400 / (27.05 * 60), 6400, 21100)
#6400 / model.vo2max(23400 / (78.6 * 60), 23400, 6400) / 60
21100 / model.vo2max(6400 / (27.05 * 60), 6400, 21100) / 60
#21100 / model.vo2max(6400 / (c(27.05, 28, 29) * 60), 6400, 21100) / 60


getPrediction_Vo2Max <- function(old_dist, old_time, new_dist){
  old_timeS <- if (is.numeric(old_time)) old_time else factorToSeconds(old_time)
  old_timeM <-  old_timeS / 60
  voMax_old <- getVo2Max(old_dist, old_timeM)  
  
  print(paste("Prev time", old_timeM, voMax_old, sep=": "))  
  
  if(old_dist < new_dist) i <- 0.1 else i <- -0.1
  
  voMax_new <- 0
  new_time <- old_timeM
  
  while(abs(voMax_new - voMax_old) > 1 && new_time > 0 && new_time < 1440) {  
    new_time <- new_time + i  
    voMax_new <- getVo2Max(new_dist, new_time)    
    
    print(paste("New time", new_time, voMax_new, sep=": "))  
  }
  new_time
}