df.max <- data.frame(names = df$names, 
                     m6400 = pmax(df$korvemaa_6.3km, df$harku_6.3km, df$rahvajooks_6.5km, na.rm = TRUE),
                     m7100 = pmax(df$narva_7km, df$olympia_7.2km, na.rm = TRUE),
                     m8900 = pmax(df$kahesilla_8.9km, na.rm = TRUE),
                     m10000 = pmax(df$olympia_10km, df$oojooks_10km, df$tallinna_sygisjooks_10km, na.rm = TRUE),
                     m10700 = pmax(df$pyhajarve_10.7km, na.rm = TRUE),
                     m13800 = pmax(df$rahvajooks_13.6km, df$ylemiste_14km, na.rm = TRUE),
                     m16000 = pmax(df$korvemaa_16km, na.rm = TRUE),
                     m21097 = pmax(df$tallinna_sygisjooks_21.1km, df$oojooks_21.1km, df$narva_21.1km, na.rm = TRUE),
                     m23400 = pmax(df$otepaa_23.4km, na.rm = TRUE),
                     m42195 = pmax(df$tallinna_sygisjooks_42.2km, na.rm = TRUE)
                    )

head(df.max)

