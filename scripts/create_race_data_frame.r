create.race.data.frame <- function(filenames, race.names, distances, first.names.first, temps, year, numOfRaces = "ALL") {
  if (numOfRaces == "ALL") {
    numOfRaces <- length(filenames)    
  } 
  #numOfRaces <- 12
  print(numOfRaces)
  
  names <- vector(mode = "numeric")
  
  races <- sapply(1:numOfRaces, function(i, year) {
    if (is.na(filenames[i]))
      race <- NA
    else {
    
      #filenames <- c( "2013.08.20.tartu_suvejooks.10km.txt", "2013.08.20.tartu_suvejooks.10km.txt")
      #i<-1
      print(filenames[i])
      race <- clean.results(read.results(filenames[i]))
      if (!first.names.first[i])
        race$Nimi <- reverse.name(race$Nimi)
      race$avg.speed <- get.avg.speed(race$seconds, distances[i])
      race$Nimi <- paste(get.sex(race), race$Nimi, year)
    }
    return(race)
  }, year)
  
  print("races generated")
  
  duplicate.names <- unlist(sapply(races, function(race) {
    get.duplicate.names(as.data.frame(race))
  }))
  
  names <- unique(unlist(sapply(races, function(race) {
    if (is.data.frame(race))
      race$Nimi
    else
      NA
  })))
  
  names <- names[! names %in% duplicate.names]  
  
  print("duplicate names removed")

  df <- data.frame(names, stringsAsFactors = F)
  for (i in 1:numOfRaces) {
    
    col <- paste(race.names[i], paste(distances[i], "km", sep = ""), sep = "_")
    m <- paste(i, col, race.names[i], paste(distances[i]))
    print (m)
    if (!is.na(filenames[i])) {
      race <- as.data.frame(races[i])
      speeds <- get.avg.speeds(df$names, race)
      if (length(temps) > 1 || !is.na(temps))
        speeds <- normalize.pace(speeds, df$names, temps[i])
      df[[col]] <- speeds
    } else
      df[[col]] <- rep(NA, nrow(df))
  }
  
  df
}
